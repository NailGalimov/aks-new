<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>

<section class="section hero_section hero_section--page  --page6 --page8">

	<div class="hero_section__img hero_section__img--page">
		<img src="../assets/images/pages/page_10/page-hero-img.png" alt="Газовое оборудование">
	</div>

	<div class="site-container">
		<div class="main-title main-title--page">
			<h1>
				Комплектуем <br> строительные объекты <span>газовым оборудованием от&nbsp;мировых производителей</span>
			</h1>

			<ul class="hero_section__list">
				<li class="hero_section__item">2000+ наименований продукции оптом и&nbsp;в&nbsp;розницу</li>
				<li class="hero_section__item">Экспресс-доставка в&nbsp;любую точку России</li>
			</ul>
		</div>

		<div class="btn_wrap btn_wrap--page">
			<button class="button" data-popup="consultation">Получить консультацию</button>
			<p>Получите бесплатную консультацию инженера по подбору насосного оборудования</p>
		</div>
	</div>
</section>

<section class="section page--section-two page6--section-two page8--section-two">
	<div class="page8--section-two__img --first">
		<img src="../assets/images/pages/page_10/land.png" alt="Земля">
	</div>
	<div class="page8--section-two__img --second">
		<img src="../assets/images/pages/page_10/air1.png" alt="Самолет">
	</div>
	<div class="page8--section-two__img --third">
		<img src="../assets/images/pages/page_10/air2.png" alt="Самолет">
	</div>
	<div class="site-container">
		<div class="main-title">
			<h1>
				Купленное у&nbsp;нас оборудование <span>обеспечит Тверь газом на&nbsp;100 лет</span>
			</h1>
		</div>

		<p class="page--section-two__paragraph">
			Тысячи отечественных предприятий
			уже сэкономили на&nbsp;эксплуатационных расходах и&nbsp;обезопасили производства
			за&nbsp;счет покупки и&nbsp;установки нашего промышленного газового оборудования высокого качества.
			<br> Если весь этот объем оборудования
			был&nbsp;бы разом поставлен в&nbsp;Тверь, город
			был&nbsp;бы обеспечен газом на&nbsp;целый век.
		</p>
	</div>
</section>

<section class="section reasons --page2 reasons--page4">

	<div class="positioned_block"></div>

	<div class="positioned_block2">
		<div class="arrows_red"></div>
	</div>

	<div class="anim_arrows"></div>

	<div class="site-container">

		<div class="main-title">
			<h2>
				6&nbsp;причин: почему нас <span>советуют директора, прорабы и&nbsp;снабженцы</span>
			</h2>
		</div>

		<div class="feautures__inner" style="background-image: url(../assets/images/feautures_bg.jpg)">

			<div class="feautures__list">
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="300">300</span> <span class="text_m">поставщиков</span></h3>
					<p>благодаря дилерским соглашениям продукция дешевле рынка – от 5% до 30%</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="12000">12 000</span><span class="text_m">объектов</span></h3>
					<p>укомплектовали под ключ: от квартиры до микрорайона и строительной площадки</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="80">80</span> <span class="color-red" >+</span><span class="text_m">сотрудников</span></h3>
					<p>площадь собственного склада мощностью 9 600 палетомест для хранения продукции</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="5000">5000</span> <span class="text_m">единиц</span></h3>
					<p>сертифицированного оборудования в каталоге компании </p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="11">11</span><span class="color-red"> лет</span> <span class="text_m">на рынке</span></h3>
					<p>благодаря огромному опыту работаем быстрее конкурентов и гарантируем результат</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="100">100</span> <span class="text_m">производителей</span></h3>
					<p>поставляем продукцию напрямую с отечественных и зарубежных заводов</p>
				</div>
			</div>

		</div>

	</div>
</section>

<section class="section worlwide worlwide--page4 worlwide--page6 worlwide--page7 worlwide--page8">

	<div class="map">
		<?php include $_SERVER['DOCUMENT_ROOT']."../app/include/pages/map.php";?>
	</div>

	<div class="site-container">
		<div class="main-title">
			<h2><span>Федеральный поставщик</span> газового оборудования</h2>
		</div>


		<div class="worlwide__text">
			<div class="worlwide__text_img">
				<img src="../assets/images/moon.png" alt="">
			</div>

			<p class="js--show-text">
				<strong><span>ООО &laquo;АрмКомплексСнаб&raquo;&nbsp;&mdash;</span> подрядчик по&nbsp;продаже и&nbsp;комплексным поставкам промышленного</strong>
				и&nbsp;бытового газового оборудования для строительных объектов&nbsp;&mdash; оптом и&nbsp;в&nbsp;розницу. Компания предлагает широкий ассортимент продукции отечественного и&nbsp;импортного производства для подачи и&nbsp;транспортировки газа.
				Благодаря 11-летнему опыту работы и&nbsp;партнерским отношениям с&nbsp;мировыми производителями мы&nbsp;имеем высокую компетенцию и&nbsp;подберем оптимальный комплекс оборудования. Всегда в&nbsp;наличии такие устройства, как газорегуляторные установки/ шкафные пункты, фильтры для очистки газа в&nbsp;газопроводах, фланцевые узлы, корректоры объема газа, приборы учета и&nbsp;системы автоматического контроля загазованности.
			</p>

			<div class="show_more_text js--read">
				<span class="show_more_text__showen">Читать далее</span>
				<span class="show_more_text__hidden">Скрыть</span>
			</div>

			<ul>
				<li>Гарантия на&nbsp;всю продукцию&nbsp;&mdash; от&nbsp;1&nbsp;года до&nbsp;5&nbsp;лет</li>
				<li>Устройства имеют сертификацию в&nbsp;системе ГОСТ</li>
				<li>Индивидуальная комплектация объекта</li>
			</ul>

		</div>

	</div>
</section>

<section class="section page_catalog page_catalog--page4 page_catalog--page6">
	<div class="site-container">
		<div class="main-title rest">
			<h2>
				2&nbsp;000+ товаров в&nbsp;каталоге <span>в&nbsp;наличии и&nbsp;под заказ</span>
			</h2>
			<p>
				<strong>Закажите полный комплект оборудования напрямую</strong> от&nbsp;производителей. Приобретите в&nbsp;нашем интернет-магазине все виды продукции по&nbsp;более доступному прайсу, чем на&nbsp;других сайтах
			</p>
		</div>

		<?
			$product_arr = ["Трубы полиэтиленовые ГАЗ", "Газорегуляторное оборудование", "Краны шаровые", "Фитинги полиэтиленовые  электросварные", "Приборы учета газа", "Системы автоматического контроля загазованности", "Трубы в ВУС изоляции"];
			$num = 0;
		?>

		<div class="main_list">
			<? foreach ($product_arr as $val) {?>
				<? $num++; ?>
				<div class="main_list__col">
					<a href="cart.php" class="main_list__item" style="background-image: url(../assets/images/pages/page_10/cart_img<? echo $num; ?>.jpg)">
						<div class="main_list__item_text">
							<p class="main_list__text"><? echo $val; ?></p>
							<p class="main_list__text--hidden">
								Текст-описание
							</p>

						</div>

						<div class="our_products__item_btn item_btn">
							<span class="item_btn--hover" data-text-2="Подоробнее" data-text="ОТ 17 000 РУБЛЕЙ" >Подробнее</span>
							<div class="item_btn__icon">
								<span class="icon-right-arrow arrow-right"></span>
								<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
									<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
								</svg>
							</div>
						</div>
					</a>
				</div>
			<? } ?>
			<div class="main_list__col last-child">
				<a href="catalog.php" class="main_list__item">
					<div class="main_list__cube">
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
					</div>

					<div class="main_list__show_all">
						Смотреть все решения
					</div>
				</a>
			</div>
		</div>

	</div>
</section>

<section class="section clients">

	<div class="main-title">
		<h2>
			<span>Продукция производства </span>
			мировых брендов
		</h2>
	</div>

	<div class="clients__wrapper">
		<?php include $_SERVER['DOCUMENT_ROOT']."../app/include/pages/clients.php";?>

		<a href="" class="button-arrow clients__btn">
			<span>Посмотреть все</span>
			<div class="icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
					<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"></path>
				</svg>
			</div>
		</a>
	</div>

</section>

<section class="section portfolio portfolio--page portfolio--page6">
	<div class="portfolio__bottom_line"></div>
	<div class="portfolio__img">
		<img src="../assets/images/porfolio_map.png" alt="">
	</div>


	<div class="site-container">
		<div class="main-title rest">
			<h2><span>Реализованные</span> под ключ проекты</h2>
			<p>
				Мы&nbsp;получаем сотни хороших отзывов от&nbsp;заказчиков, которые приобрели оборудование с&nbsp;нашего сайта.
			</p>
		</div>

		<div class="custom-arrows portfolio__arrow-container"></div>


		<?php include $_SERVER['DOCUMENT_ROOT']."../app/include/pages/portfolio-slider.php";?>

		<div class="text-center">
			<a class="portfolio__see_all" href="projects.php">Посмотреть все объекты</a>
		</div>

	</div>
</section>

<section class="section footer_section footer_section--page">

	<div class="footer_section__img footer_section--page__img">
		<img src="../assets/images/page-footer-img.png" alt="">
	</div>

	<div class="site-container">
		<div class="footer_section__row">
			<div class="footer_section__col">
				<div class="footer_section__item">
					<div class="main-title">
						<h2>Получите расчет стоимости <span>поставки</span></h2>
					</div>

					<ul>
						<li><strong>Подберем оптимальный вариант оборудования</strong> для комплектации вашего объекта.</li>
						<li><strong>Предложим 3 варианта комплектации</strong> объекта в рамках бюджета.</li>
					</ul>
				</div>
			</div>
			<div class="footer_section__col">
				<div class="footer_section__item">
					<div class="form_custom">
						<form action="" class="formValidate validate" name="main-form">
							<h5>Оставьте заявку,</h5>
							<p>мы свяжемся с вами по телефону, поможем вам выбрать трубы
								с оптимальными характеристиками
								и рассчитаем стоимость покупки
							</p>

							<div class="input_wrapper">
								<div class="input_container">
									<input type="text" placeholder="Ваше имя" name="name" class="required" data-mask="fio">
								</div>
								<div class="input_container">
									<input type="text" placeholder="Ваш номер" name="tell" class="required" data-mask="phone">
								</div>
								<div class="input_container">
									<input type="text" placeholder="Ваша почта" name="e-mail" class="required" data-mask="email">
								</div>
							</div>

							<div class="text-center">
								<button class="button" type="submit">Оставить заявку</button>
							</div>

							<label class="form-agreement">
								<input class="form-agreement__input required" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
								<span class="form-agreement__text">
									<span class="form-agreement__check"></span>
									Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
								</span>
							</label>

						</form>
					</div>
				</div>
			</div>
		</div>





	</div>
</section>

<? include $_SERVER['DOCUMENT_ROOT'].'../app/html/footer.php' ?>
