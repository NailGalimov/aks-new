<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
<div class="site-container">
    <? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
</div>

<section class="section projects">
    <div class="projects__image">
        <img src="../assets/images/pages/projects/hero-img.png" alt="Завод">
    </div>
    <div class="site-container">
        <div class="projects__title main-title">
            <h1>
                Укомплектовали под ключ <span>12&nbsp;000 Объектов по&nbsp;России и&nbsp;СНГ</span>
            </h1>

            <p>
                Полная комплектация инженерными сетями проектов любого масштаба: от&nbsp;квартиры до&nbsp;микрорайона
                и&nbsp;строительной площадки
            </p>
        </div>

        <div class="btn_wrap">
            <button class="button" data-popup="consultation">Получить консультацию</button>
            <p>Получите консультацию технического специалиста бесплатно</p>
        </div>
    </div>
</section>

<section class="section cases">
    <div class="site-container">
        <ul class="cases__list" id="cases__list">

            <?
					$arr = ["Аквапарк «Лазурный»", "Токаревская птицефабрика", "Реализация программы «Газпром детям»", "Комбикормовый завод ГК «АГРОЭКО»"];
				?>

            <? foreach ($arr as $val) {?>

            <li class="cases__item">
                <div class="cases__item--left">
                    <h3 class="cases__item-title">
                        <? echo $val; ?>
                    </h3>

                    <p class="cases__item-subtitle">
                        п. Разумное, Белгородская область <span>2019г</span>
                    </p>

                    <article class="cases__item-text">
                        <span class="cases__item-text-legend">
                            Задача:
                        </span>

                        <p class="cases__item-text-info">
                            поставка трубопровода и&nbsp;запорной арматуры на&nbsp;крупномасштабный объект Белгородской
                            области. Наряду с&nbsp;подбором качественного материала важны были сроки поставки.
                        </p>

                        <span class="cases__item-text-legend">
                            Результат:
                        </span>

                        <p class="cases__item-text-info">
                            Краны, задвижки и&nbsp;затворы европейских брендов, поставленные на&nbsp;объект, отличаются
                            своим качеством и&nbsp;гарантийным периодом эксплуатации.
                        </p>
                    </article>

                    <span class="cases__item-price-title">
                        Стоимость:
                    </span>
                    <div class="cases__item-cost">
                        <span class="cases__item-cost-price">
                            1,6 млрд рублей
                        </span>

                        <span class="cases__item-cost-text">
                            Инвестиции в проект
                        </span>
                    </div>

                </div>
                <div class="cases__item--right">
                    <div class="cases__item-img">
                        <img src="../assets/images/pages/projects/projects-img1.jpg" alt="Аквапарк «Лазурный»">
                    </div>
                </div>
            </li>

            <? } ?>
        </ul>
        <div class="text-center">
            <a href="" class="button-arrow cases__button-show">
                <span>Еще</span>
                <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
                        <path id="icon-arrow"
                            d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z"
                            transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f"
                            stroke-width="2"></path>
                    </svg>
                </div>
            </a>
        </div>

    </div>
</section>

<section class="section footer_section footer_section--page projects-form">

	<div class="projects-form__img --calc">
		<img src="../assets/images/pages/projects/calc.png" alt="Калькулятор">
	</div>

	<div class="projects-form__img --tube">
		<img src="../assets/images/pages/projects/tube.png" alt="Трубы">
	</div>

    <div class="site-container">
        <div class="footer_section__row">
            <div class="footer_section__col">
                <div class="footer_section__item">
                    <div class="main-title">
                        <h2>Получите расчет стоимости <span>проекта</span></h2>
                    </div>

                    <ul>
                        <li><strong>Подберем выгодное коммерческое предложение</strong> под вашу задачу.
                        </li>
                        <li><strong>Предложим 3 варианта комплектации</strong> объекта в рамках бюджета.</li>
						<li>
							<strong>Рассчитаем проект с учетом отсрочки</strong> платежа – по договоренности.
						</li>
                    </ul>
                </div>
            </div>
            <div class="footer_section__col">
                <div class="footer_section__item">
                    <div class="form_custom">
                        <form action="" class="formValidate validate" name="main-form" novalidate="novalidate">
                            <h5>Оставьте заявку,</h5>
                            <p>мы свяжемся с вами по телефону, поможем вам выбрать трубы
                                с оптимальными характеристиками
                                и рассчитаем стоимость покупки
                            </p>

                            <div class="input_wrapper">
                                <div class="input_container">
                                    <input type="text" placeholder="Ваше имя" name="name" class="required"
                                        data-mask="fio">
                                </div>
                                <div class="input_container">
                                    <input type="text" placeholder="Ваш номер" name="tell" class="required"
                                        data-mask="phone">
                                </div>
                                <div class="input_container">
                                    <input type="text" placeholder="Ваша почта" name="e-mail" class="required"
                                        data-mask="email">
                                </div>
                            </div>

                            <div class="text-center">
                                <button class="button" type="submit">Оставить заявку</button>
                            </div>

							<label class="form-agreement">
								<input class="form-agreement__input required" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
								<span class="form-agreement__text">
									<span class="form-agreement__check"></span>
									Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
								</span>
							</label>

                        </form>
                    </div>
                </div>
            </div>
        </div>





    </div>
</section>

<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
