<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta id="myViewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<title>АКС</title>

    <link rel="stylesheet" href="../assets/css/base.min.css">
	<link rel="stylesheet" href="../assets/css/libs.style.css">
    <link rel="stylesheet" href="../assets/fonts/icomoon/style.css">
    <link rel="stylesheet" href="../assets/css/style.css">
</head>

<body class="body-custom">
	<header class="header_main">

		<div class="mobile_burger hidden js--burger">
				<div class="menu_burger">
					<span></span>
					<span></span>
					<span></span>
				</div>
		</div>
		<div class="header-navigation">
			<div class="header_main__item  js--burger">
				<div class="menu_burger menu_burger_main">
					<span></span>
					<span></span>
					<span></span>
				</div>
				меню
			</div>

			<a href="search.php" class="header_main__item search">
				<div class="icon">
					<span class="icon-search"></span>
				</div>
				поиск
			</a>

			<a href="" class="header_main__item basket-cart">
				<div class="icon">
					<span class="icon-basket"></span>
					<div class="basket-cart-count">
						33
					</div>
				</div>
				корзина
			</a>

			<a href="" class="header_main__item user js--user-cabinet">
				<div class="icon">
					<span class="icon-user"></span>
				</div>
				кабинет
			</a>


			<a data-scroll-top href="#header_custom" class="header_main__top">
				<svg xmlns="http://www.w3.org/2000/svg" width="18.741" height="41.707" viewBox="0 0 18.741 41.707">
					<g id="arrow-top" data-name="arrow-top" transform="translate(-41.129 -834.793)">
						<line id="arrow-top-line" data-name="arrow-top-line" y2="41" transform="translate(50.5 835.5)" fill="none" stroke="#fff" stroke-width="1"/>
						<path id="arrow-top-line2" data-name="arrow-top-line2" d="M0,0H12.752V12.752" transform="translate(41.483 844.517) rotate(-45)" fill="none" stroke="#fff" stroke-width="1"/>
					</g>
				</svg>
			</a>
		</div>

		<div class="header_main__info">
			<div class="site-container">
				<div class="header_main__row">
					<div class="header_main__col">
						<nav class="header_main__nav">
							<ul>
								<li class="strong"><a href="about.php">О компании</a></li>
								<li class="strong"><a href="catalog.php">Каталог</a></li>
								<li class="strong"><a href="solutions.php">Решения под ключ</a></li>
								<li><a href="projects.php">Проекты</a></li>
								<li><a href="delivery.php">Доставка</a></li>
								<li><a href="clients.php">Клиентам</a></li>
								<li><a href="contacts.php">Контакты</a></li>
							</ul>
						</nav>
						<a href="" class="header_main__policy">политика конфиденциальности</a>
					</div>
					<div class="header_main__col">
						<div class="header_main__contacts header-contacts">
							<div class="header-contacts__container animElMenu">
								<span class="header-contacts__legend">
									г. Белгород
								</span>
								<div class="header-contacts__link-group">
									<a class="header-contacts__tel" href="tel:+74722568082">+7 (4722) 56-80-82</a>
									<a class="header-contacts__mail" href="mailto:acs.31@yandex.ru">acs.31@yandex.ru</a>
								</div>
							</div>
							<div class="header-contacts__container animElMenu">
								<span class="header-contacts__legend">
									г. Курск
								</span>
								<div class="header-contacts__link-group">
									<a class="header-contacts__tel" href="tel:+74712343009">+7 (4712) 34-30-09</a>
									<a class="header-contacts__mail" href="mailto:acs46@yandex.ru">acs46@yandex.ru</a>
								</div>
							</div>
							<div class="header_main__consultation animElMenu">
								<img src="../assets/images/menu-phone.png" alt="Телефон">
								<span>
									Консультация технического  специалиста бесплатно
								</span>
								<button class="button header_main__button" data-popup="consultation">
									Получить консультацию
								</button>
							</div>
							<div class="b2b-copy">
								<a href="" target="_blank" class="b2b-copy-link">Разработано</a>	<a href="" target="_blank" class="b2b-copy-svg">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 316.1 99" role="img">
										<title>маркетинговое агентство </title>
										<circle cx="49.5" cy="49.5" r="49.5" fill="#fa0"></circle>
										<path fill="#1D1A24" d="M53.2 46.9l-2.8-9.6L9.5 78.7l16.4-11.9 20-14.5 2.3 7.8 41.4-39.6-36.4 26.4zm29.2-3.5L66.7 58.7l15.5-10.8 7.5 5.8-7.3-10.3zm-55.9 0l-19.3 20L26.4 49l9.8 7.1-9.7-12.7z"></path>
										<path id="b2b" d="M239 79.6h10.3v-9.8L239 79.6zm7-11.1l-25.9.1c.1-3.8 6-6.9 12.9-11.1 7.6-4.5 15.8-10.4 15.8-20.2 0-11.9-9.5-17.2-21.3-17.2-7.2-.1-14.2 2-20.1 6.2v12c6.2-4.7 13-6.9 18.9-6.9s9.7 2.2 9.7 6.5c0 5.6-5.2 7.9-11.3 11.4-8.6 5.1-19.2 10.5-19.2 24.6v5.7h25.3L246 68.5zM140.3 22.2l4.1 14.2 23.1-16.8c-.3 0-.7 0-1 0h-26.3l.1 2.6zm37.9 25.6c5.3-1.8 9.1-6.1 9.1-13.1 0-8-4.9-12.8-13.2-14.5L163.8 30h1.6c5.7 0 9.1 2.1 9.1 6.9s-3.5 6.9-8.4 6.9h-12.9v-3.5l-12.9 12.3v27h25.8c10.5 0 23.6-3.2 23.6-16.8 0-8.9-5.6-13.3-11.5-15zm-12.3 21.3H153V53.7h13.5c5.1 0 10.2 1.4 10.2 7.7.1 6.5-5 7.7-10.8 7.7zm100.8-46.9l4.1 14.2L294 19.6c-.3 0-.7 0-1 0h-26.3v2.6zm37.9 25.6c5.3-1.8 9.1-6.1 9.1-13.1 0-8-4.9-12.8-13.2-14.5L290.2 30h1.6c5.7 0 9.1 2.1 9.1 6.9s-3.5 6.9-8.4 6.9h-12.9v-3.5l-12.9 12.3v27h25.8c10.5 0 23.6-3.2 23.6-16.8 0-8.9-5.6-13.3-11.5-15zm-12.3 21.3h-12.9V53.7H293c5.1 0 10.2 1.4 10.2 7.7 0 6.5-5.1 7.7-10.9 7.7z"></path>
									</svg>
								</a>
							</div>
							<a href="" class="header_main__policy">политика конфиденциальности</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="basket-mini">
			<div class="basket-mini__container">
				<ul class="basket-mini__list" data-simplebar>
					<li class="basket-mini__item" data-id="1">
						<a class="basket-mini__img" href="">
							<img src="../assets/images/mini-cart-img1.jpg" alt="Задвижка стальная 30с41нж Ру16  (ЗКЛ-2-16) МЗТА">
						</a>
						<h3 class="basket-mini__legend">
							Задвижка стальная 30с41нж Ру16  (ЗКЛ-2-16) МЗТА
						</h3>
						<span class="icon-cross basket-mini__delete"></span>
					</li>
					<li class="basket-mini__item" data-id="1">
						<a class="basket-mini__img" href="">
							<img src="../assets/images/mini-cart-img2.jpg" alt="Отводы ГОСТ 17375-2001">
						</a>
						<h3 class="basket-mini__legend">
							Отводы ГОСТ 17375-2001
						</h3>
						<span class="icon-cross basket-mini__delete"></span>
					</li>
					<li class="basket-mini__item" data-id="1">
						<a class="basket-mini__img" href="">
							<img src="../assets/images/mini-cart-img3.jpg" alt="Краны латунные шаровые  11б27п (газ)">
						</a>
						<h3 class="basket-mini__legend">
							Краны латунные шаровые  11б27п (газ)
						</h3>
						<span class="icon-cross basket-mini__delete"></span>
					</li>
					<li class="basket-mini__item" data-id="1">
						<a class="basket-mini__img" href="">
							<img src="../assets/images/mini-cart-img3.jpg" alt="Краны латунные шаровые  11б27п (газ)">
						</a>
						<h3 class="basket-mini__legend">
							Краны латунные шаровые  11б27п (газ)
						</h3>
						<span class="icon-cross basket-mini__delete"></span>
					</li>
				</ul>
				<div class="basket-mini__descr">
					<div class="basket-mini__row">
						<span class="basket-mini__name">
							Товары
						</span>
						<span class="basket-mini__price">
							<span class="basket-mini--start-price">110 000</span><span class="icon-ruble"></span>
						</span>
					</div>
					<div class="basket-mini__row">
						<span class="basket-mini__name">
							Скидка на товар
						</span>
						<span class="basket-mini__price basket-mini__price--discount">
							-1000
						</span>
					</div>
					<div class="basket-mini__row">
						<span class="basket-mini__name">
							Итого:
						</span>
						<span class="basket-mini__price basket-mini__price--big-font">
							<span class="basket-mini--final-price">109 000</span> <span class="icon-ruble"></span>
						</span>
					</div>
				</div>
				<a href="basket.php" class="button-arrow basket-mini__button">
					<span>Перейти в корзину</span>
					<div class="icon">
						<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
							<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"></path>
						</svg>
					</div>
				</a>
			</div>
		</div>

		<div class="cabinet">
			<div class="cabinet__container">
				<div class="cabinet__tabs">
					<div class="cabinet__personal tabs" data-cab-item="personal">
						<h3 class="cabinet__title">
							Личный кабинет
						</h3>
						<div class="cabinet__content">
							<div class="cabinet__mini-tabs">
								<div class="cabinet__mini-item" data-show="legal">
									Юр. лицо
								</div>
								<div class="cabinet__mini-item active" data-show="private">
									Частное лицо
								</div>
							</div>
							<div class="cabinet__legal" data-item="legal">
								<form class="cabinet__legal-form formValidate validate" name="legal-form">
									<div class="cabinet__input-container">
										<input type="text" data-mask="email" class="cabinet__input required" placeholder="Email" name="email">
									</div>
									<div class="cabinet__input-container">
										<input type="password" class="cabinet__input required" placeholder="Пароль" name="password">
										<span class="icon-icon-eye js-show-pass"></span>
									</div>
									<button class="button-arrow cabinet__button" type="submit">
										<span>Войти</span>
										<i class="icon">
											<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
												<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"></path>
											</svg>
										</i>
									</button>
								</form>
							</div>
							<div class="cabinet__private active" data-item="private">
								<form class="cabinet__private-form formValidate validate" name="private-form">
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" placeholder="Логин" name="login">
									</div>
									<div class="cabinet__input-container">
										<input type="password" class="cabinet__input required" placeholder="Пароль" name="password">
										<span class="icon-icon-eye js-show-pass"></span>
									</div>
									<button class="button-arrow cabinet__button" type="submit">
										<span>Войти</span>
										<i class="icon">
											<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
												<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"></path>
											</svg>
										</i>
									</button>
								</form>
							</div>
						</div>
						<div class="cabinet__additionals">
							<a href="" data-popup="forgot-password" class="cabinet__forgot-password">Забыли пароль?</a>
							<a href="javascript:void(0)" data-cab-show="registration" class="cabinet__new-acc js--change-cab">Создать новый аккаунт</a>
						</div>
					</div>
					<div class="cabinet__registration tabs" data-cab-item="registration">
						<h3 class="cabinet__title">
							Регистрация
						</h3>
						<div class="cabinet__content" data-simplebar>
							<div class="cabinet__mini-tabs">
								<div class="cabinet__mini-item" data-show="registration-legal">
									Юр. лицо
								</div>
								<div class="cabinet__mini-item active" data-show="registration-private">
									Частное лицо
								</div>
							</div>
							<div class="cabinet__legal" data-item="registration-legal">
								<form class="cabinet__private-form formValidate validate" name="legal-reg">
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" placeholder="Наименование организации" name="company-name">
									</div>
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" data-mask="innUR" placeholder="ИНН" name="inn">
									</div>
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" data-mask="kpp" placeholder="КПП" name="kpp">
									</div>
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" data-mask="email" placeholder="Электронная почта" name="legal-reg-emal">
									</div>
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" placeholder="Логин" name="legal-reg-login">
									</div>
									<div class="cabinet__input-container">
										<input type="password" class="cabinet__input required logInPass" placeholder="Пароль" data-mask="samePass1" name="legal-password-first">
										<span class="icon-icon-eye js-show-pass"></span>
									</div>
									<div class="cabinet__input-container">
										<input type="password" class="cabinet__input required logInPass1" placeholder="Повторите пароль" data-mask="samePass" name="legal-password-second">
										<span class="icon-icon-eye js-show-pass"></span>
									</div>
									<button class="button-arrow cabinet__button" type="submit">
										<span>Зарегестрироваться</span>
										<i class="icon">
											<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
												<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"></path>
											</svg>
										</i>
									</button>
								</form>
							</div>
							<div class="cabinet__private active" data-item="registration-private">
								<form class="cabinet__private-form formValidate validate" name="private-reg">
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" placeholder="Имя" name="name">
									</div>
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" placeholder="Фамилия" name="second-name">
									</div>
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" data-mask="phone" placeholder="Телефон" name="tel">
									</div>
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" data-mask="email" placeholder="Электронная почта" name="private-reg-emal">
									</div>
									<div class="cabinet__input-container">
										<input type="text" class="cabinet__input required" placeholder="Логин" name="private-reg-login">
									</div>
									<div class="cabinet__input-container">
										<input type="password" class="cabinet__input required logInPass" placeholder="Пароль" data-mask="samePass1" name="password-first">
										<span class="icon-icon-eye js-show-pass"></span>
									</div>
									<div class="cabinet__input-container">
										<input type="password" class="cabinet__input required logInPass1" placeholder="Повторите пароль" data-mask="samePass" name="password-second">
										<span class="icon-icon-eye js-show-pass"></span>
									</div>
									<button class="button-arrow cabinet__button" type="submit">
										<span>Зарегестрироваться</span>
										<i class="icon">
											<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
												<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"></path>
											</svg>
										</i>
									</button>
								</form>
							</div>
						</div>
						<div class="cabinet__additionals">
							<a href="javascript:void(0)" data-cab-show="personal" class="cabinet__new-acc js--change-cab">Войти в свой аккаунт</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</header>
	<!-- для страницы корзины отключаем overflow: hidden; чтобы работал position: sticky; -->
	<main class="main_custom <?if (basename($_SERVER['PHP_SELF']) === "basket.php") echo "overflow-visible"?> ">

	<div class="header_custom" id="header_custom">

	<div class="site-container">
		<div class="header_custom__inner">

			<a href="" class="header_custom__logo">
				<img src="../assets/images/logo.png" alt="logo">
			</a>

			<p>Снабжение строительных объектов инженерными системами и оборудованием</p>

			<!-- для страницы поиска убираем поиск в шапке -->
			<div class="header_custom__form search_form" <?if (basename($_SERVER['PHP_SELF']) === "search.php") echo "style='display: none;'"?> >
				<form class="formValidate validate" name="header-search-form">
					<input class="required" type="text" placeholder="Найти" name="search-result">
					<button type="submit" class="button-reset icon"><span class="icon-search"></span></button>
				</form>
			</div>

			<div class="header_custom__links">
				<a href="tel:+74722569082">+7 (4722) 56-80-82</a>

				<a class="order_call" href="" data-popup="order-call">Заказать звонок</a>
			</div>

		</div>
	</div>

	</div>

