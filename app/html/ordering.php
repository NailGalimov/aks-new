<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
	<div class="site-container">
		<? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
	</div>

	<section class="section ordering-page">
		<div class="site-container">
			<h1 class="ordering-page__title">
				Оформление
			</h1>

			<form class="form formValidate validate" action="" name="ordering-form">
				<h3 class="ordering-page__legend">
					Адрес
				</h3>
				<div class="ordering-page__top tabs mapChanger">
					<div class="ordering-page__left ordering-page__tabs">
						<div class="ordering-page__tabs-nav">
							<div class="ordering__delivery-row">
								<label class="ordering__delivery-item">
									<input class="ordering__delivery-radio" type="radio" id="1" name="delivery-way" value="На складе" data-show="inStock">
									<span class="ordering__delivery-box"></span>
									<span class="ordering__delivery-name">На складе</span>
									<span class="ordering__delivery-info">Сегодня, бесплатно</span>
								</label>
								<label class="ordering__delivery-item">
									<input class="ordering__delivery-radio active" checked="checked" type="radio" id="2" name="delivery-way" value="Оформить доставку" data-show="delivery">
									<span class="ordering__delivery-box"></span>
									<span class="ordering__delivery-name">Оформить доставку</span>
									<span class="ordering__delivery-info">Завтра и позже</span>
								</label>
							</div>
						</div>
						<div class="ordering-page__tabs-content active" data-item="delivery">
							<div class="ordering-page__input-wrap">
								<input type="text" class="ordering-page__input --disabled inputMap inputMap-city required" name="city" placeholder="Город" value="Белгород">
							</div>
							<div class="ordering-page__input-wrap">
								<input type="text" class="ordering-page__input inputMap inputMap-street required" name="adress" placeholder="Улица">
							</div>
							<div class="ordering-page__double-input">
								<div class="ordering-page__input-wrap --width50">
									<input type="text" class="ordering-page__input inputMap inputMap-house required" name="home" placeholder="Дом">
								</div>
								<div class="ordering-page__input-wrap --width50">
									<input type="text" class="ordering-page__input inputMap required" name="office" placeholder="Офис">
								</div>
							</div>
							<div class="ordering-page__input-wrap">
								<textarea class="ordering-page__textarea" name="text" placeholder="Комментарий для доставки"></textarea>
							</div>
						</div>
						<div class="ordering-page__tabs-content in-stock" style="display: none;" data-item="inStock">
							<p class="ordering-page__inStock-text">
								Вы можете забрать посылку на нашем складе по адресу: <br> <span>ул. Карима-Тинчурина, д. 84/1</span>
							</p>
						</div>
					</div>
					<div class="ordering-page__right">
						<div class="ordering-page__map ya_map active" id="delivery-map" data-zoom="12" data-center="50.590376, 36.626362" data-item="delivery"></div>
						<div class="ordering-page__map ya_map" id="in-stock-map" data-zoom="17" data-center="50.590376, 36.626362" style="display: none;" data-item="inStock"></div>
					</div>
				</div>
				<div class="ordering-page__bottom">
					<div class="ordering-page__left --bottom-left ordering-page__tabs tabs">
						<h3 class="ordering-page__legend">
							Получатель
						</h3>
						<div class="ordering-page__tabs-nav --bottom-nav">
							<div class="ordering__delivery-row">
								<label class="ordering__delivery-item --bottom-nav-item">
									<input class="ordering__delivery-radio" type="radio" id="legal" name="receiver" value="legal" data-show="legal">
									<span class="ordering__delivery-box"></span>
									<span class="ordering__delivery-name">Юр. лицо</span>
								</label>
								<label class="ordering__delivery-item --bottom-nav-item">
									<input class="ordering__delivery-radio active" checked="checked" type="radio" id="pesonal" name="receiver" value="pesonal" data-show="pesonal">
									<span class="ordering__delivery-box"></span>
									<span class="ordering__delivery-name">Частное лицо</span>
								</label>
							</div>
						</div>
						<div class="ordering-page__bottom-content">
							<div class="ordering-page__content-item active" data-item="pesonal">
								<div class="ordering-page__double-input">
									<div class="ordering-page__input-wrap --width50">
										<input type="text" class="ordering-page__input" name="name" placeholder="Имя">
									</div>
									<div class="ordering-page__input-wrap --width50">
										<input type="text" class="ordering-page__input" name="second-name" placeholder="Фамилия">
									</div>
								</div>
								<div class="ordering-page__double-input">
									<div class="ordering-page__input-wrap --width50">
										<input type="text" class="ordering-page__input" name="email" placeholder="Электронная почта">
									</div>
									<div class="ordering-page__input-wrap --width50">
										<input type="text" class="ordering-page__input" name="tel" placeholder="Телефон">
									</div>
								</div>
							</div>
							<div class="ordering-page__content-item" data-item="legal" style="display: none;">
								<div class="ordering-page__double-input">
									<div class="ordering-page__input-wrap --width50">
										<input type="text" class="ordering-page__input" name="compny-name" placeholder="Наименование компании">
									</div>
									<div class="ordering-page__input-wrap --width50">
										<input type="text" class="ordering-page__input" name="inn" placeholder="ИНН компании">
									</div>
								</div>
							</div>
						</div>
						<div class="ordering-page__bottom-payment">
							<h3 class="ordering-page__legend">
								Оплата
							</h3>
						</div>
					</div>
					<div class="ordering-page__right --bottom-right">
						<div class="basket-mini__container ordering-page__basket">
							<ul class="basket-mini__list ordering-page__basket-list" data-simplebar>
								<li class="basket-mini__item" data-id="1">
									<a class="basket-mini__img" href="">
										<img src="../assets/images/mini-cart-img1.jpg" alt="Задвижка стальная 30с41нж Ру16  (ЗКЛ-2-16) МЗТА">
									</a>
									<h3 class="basket-mini__legend">
										Задвижка стальная 30с41нж Ру16  (ЗКЛ-2-16) МЗТА
									</h3>
									<span class="icon-cross basket-mini__delete"></span>
								</li>
								<li class="basket-mini__item" data-id="1">
									<a class="basket-mini__img" href="">
										<img src="../assets/images/mini-cart-img2.jpg" alt="Отводы ГОСТ 17375-2001">
									</a>
									<h3 class="basket-mini__legend">
										Отводы ГОСТ 17375-2001
									</h3>
									<span class="icon-cross basket-mini__delete"></span>
								</li>
								<li class="basket-mini__item" data-id="1">
									<a class="basket-mini__img" href="">
										<img src="../assets/images/mini-cart-img3.jpg" alt="Краны латунные шаровые  11б27п (газ)">
									</a>
									<h3 class="basket-mini__legend">
										Краны латунные шаровые  11б27п (газ)
									</h3>
									<span class="icon-cross basket-mini__delete"></span>
								</li>
								<li class="basket-mini__item" data-id="1">
									<a class="basket-mini__img" href="">
										<img src="../assets/images/mini-cart-img3.jpg" alt="Краны латунные шаровые  11б27п (газ)">
									</a>
									<h3 class="basket-mini__legend">
										Краны латунные шаровые  11б27п (газ)
									</h3>
									<span class="icon-cross basket-mini__delete"></span>
								</li>
							</ul>
							<div class="basket-mini__descr">
								<div class="basket-mini__row">
									<span class="basket-mini__name">
										Товары
									</span>
									<span class="basket-mini__price">
										<span class="basket-mini--start-price">110 000</span><span class="icon-ruble"></span>
									</span>
								</div>
								<div class="basket-mini__row">
									<span class="basket-mini__name">
										Скидка на товар
									</span>
									<span class="basket-mini__price basket-mini__price--discount">
										-1000
									</span>
								</div>
								<div class="basket-mini__row">
									<span class="basket-mini__name">
										Итого:
									</span>
									<span class="basket-mini__price basket-mini__price--big-font">
										<span class="basket-mini--final-price">109 000</span> <span class="icon-ruble"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>

<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
