<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
	<div class="site-container">
		<? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
	</div>

	<section class="section delivery">
		<div class="delivery__image">
			<img src="../assets/images/pages/delivery/hero-img.png" alt="Доставка">
		</div>

		<div class="site-container">
			<div class="delivery__title main-title">
				<h1>
					Поставки оборудования для инженерных сетей <span>во&nbsp;все регионы <br> России и&nbsp;СНГ</span>
				</h1>

				<ul class="hero_section__list">
					<li class="hero_section__item">
						Доставка продукции собственным корпоративным автопарком&nbsp;&mdash; от&nbsp;1&nbsp;дня
					</li>
					<li class="hero_section__item">
						Срочная экспресс-доставка до&nbsp;объекта курьерской службой
					</li>
					<li class="hero_section__item">
						Отгрузка в&nbsp;день обращения
					</li>
				</ul>
			</div>

			<div class="btn_wrap">
				<button class="button" data-popup="price">Рассчитать стоимость</button>
				<p>Получите расчет стоимости <br> выгодной до вашего объекта</p>
			</div>
		</div>
	</section>

	<section class="section in-time">
		<div class="in-time__image">
			<img src="../assets/images/pages/delivery/delivery-boy.png" alt="Доставка грузов">
		</div>
		<div class="site-container">
			<div class="in-time__title main-title">
				<h2>
					Вы&nbsp;получите <span>заказ <br> точно в&nbsp;назначенный срок</span>
				</h2>

				<p>
					Опытный отдел логистики организует доставку оборудования на&nbsp;ваш объект в&nbsp;любую точку страны&nbsp;&mdash; оптимально по&nbsp;цене и&nbsp;срокам.
				</p>
			</div>

			<ul class="in-time__list">
				<li class="in-time__item">
					Оперативная доставка собственным транспортом по&nbsp;России&nbsp;&mdash; от&nbsp;1&nbsp;дня.
				</li>
				<li class="in-time__item">
					Оперативная отгрузка товара в&nbsp;день обращения и&nbsp;выгрузка товара на&nbsp;объекте.
				</li>
				<li class="in-time__item">
					Экспресс-доставка транспортными компаниями во&nbsp;все регионы РФ&nbsp;&mdash; от&nbsp;1&nbsp;дня.
				</li>
				<li class="in-time__item">
					Перевозка негабаритных грузов. Различные операторы разногабаритных грузов.
				</li>
			</ul>

			<p class="in-time__text">
				Благодаря тому, что у&nbsp;нас налажено адресное хранение,
				которое исключает пересортицу, а&nbsp;сам склад товаров оборудован козловым краном и&nbsp;работает круглосуточно, мы&nbsp;можем отгрузить
				партию оборудования в&nbsp;любое время.
			</p>

			<p class="in-time__text">
				В&nbsp;наличии на&nbsp;складе более 20&nbsp;000 единиц продукции,
				реализована программа управления запасами, поэтому скорость выполнения заказов у&nbsp;нас выше и&nbsp;качественнее, чем у&nbsp;конкурентов.
			</p>
		</div>
	</section>

	<section class="section delivery-cars">
		<div class="site-container">
			<div class="delivery-cars__title main-title">
				<h2>
					Собственный автопарк <span>для доставки в&nbsp;радиусе 500&nbsp;км</span>
				</h2>
			</div>
		</div>

			<?
				$arr = ["8 автомобилей 1,5 т", "6 пятитонников", "5 малых курьерских машин"];
				$num = 0;
			?>

			<ul class="delivery-cars__list">
				<? foreach ($arr as $val) {?>
				<? $num++; ?>
					<li class="delivery-cars__item">
						<img class="delivery-cars__img" src="../assets/images/pages/delivery/delivery-car<?echo $num;?>.png" alt="">
						<span class="delivery-cars__text">
							<? echo $val; ?>
						</span>
					</li>
				<? } ?>
			</ul>
			<div class="site-container">
				<p class="delivery-cars__info">
					Доставка по&nbsp;ЦФО (Центральный Федеральный округ) и&nbsp;СЗО (Северо-Западный федеральный округ) включена в&nbsp;стоимость.
				</p>
			</div>
	</section>

	<section class="section footer_section footer_section--page delivery-form">
		<div class="delivery-form__img">
			<img src="../assets/images/pages/delivery/delivery-form-img.png" alt="Автопарк">
		</div>

		<div class="site-container">
			<div class="footer_section__row">
				<div class="footer_section__col">
					<div class="footer_section__item">
						<div class="main-title">
							<h2>Доставка <span>в&nbsp;другие регионы рассчитывается нашим логистом</span></h2>
							<p>
								Подберем самый оптимальный и&nbsp;быстрый вариант доставки в&nbsp;течение 20&nbsp;минут.
							</p>
						</div>

						<span class="delivery-form__legend">
							Транспортные компании:
						</span>

						<ul class="delivery-form__list">
							<? $num2 = 0; ?>
							<? for($i=0; $i<4; $i++) { ?>
								<? $num2++; ?>
								<li class="delivery-form__item">
									<img src="../assets/images/pages/delivery/company-img<? echo $num2; ?>.png" alt="СДЕК">
								</li>
							<? } ?>
						</ul>


					</div>
				</div>
				<div class="footer_section__col">
					<div class="footer_section__item">
						<div class="form_custom">
							<form action="" class="formValidate validate" name="main-form" novalidate="novalidate">
								<h5>Оставьте заявку,</h5>
								<p>мы свяжемся с вами по телефону, поможем вам выбрать трубы
									с оптимальными характеристиками
									и рассчитаем стоимость покупки
								</p>

								<div class="input_wrapper">
									<div class="input_container">
										<input type="text" placeholder="Ваше имя" name="name" class="required"
											data-mask="fio">
									</div>
									<div class="input_container">
										<input type="text" placeholder="Ваш номер" name="tell" class="required"
											data-mask="phone">
									</div>
									<div class="input_container">
										<input type="text" placeholder="Ваша почта" name="e-mail" class="required"
											data-mask="email">
									</div>
								</div>

								<div class="text-center">
									<button class="button" type="submit">Оставить заявку</button>
								</div>

								<label class="form-agreement">
									<input class="form-agreement__input required" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
									<span class="form-agreement__text">
										<span class="form-agreement__check"></span>
										Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
									</span>
								</label>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
