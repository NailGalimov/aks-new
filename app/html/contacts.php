<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
	<div class="site-container">
		<? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
	</div>

	<section class="section contacts">
		<div class="site-container">
			<div class="contacts__title">
				<h1>
					<span>Компания</span> "АрмКомплексСнаб"
				</h1>
			</div>

			<div class="contacts__row">
				<div class="contacts__col">
					<div class="contacts__info">
						<span class="contacts__city-name">
							г.Белгород
						</span>

						<div class="contacts__company-info-item company-info">
							<span class="company-info__legend">
								Режим работы
							</span>
							<span class="company-info__text">
								Пн.–пт.: с 8:00 до 18:00
							</span>
						</div>
						<div class="contacts__company-info-item company-info">
							<span class="company-info__legend">
								Телефон:
							</span>
							<a href="tel:+74722777152" class="company-info__text">
								+7 (4722) 77-71-52
							</a>
						</div>
						<div class="contacts__company-info-item company-info">
							<span class="company-info__legend">
								E-mail:
							</span>
							<a href="" class="company-info__text contacts__email">
								acs.31@yandex.ru
							</a>
						</div>
						<adress class="contacts__company-info-item company-info company-info__adress">
							<span class="company-info__legend">
								Адреса:
							</span>
							<span class="company-info__text">
								г. Белгород, ул. Корочанская, 132-А, 3&nbsp;этаж, оф.&nbsp;13
							</span>
						</adress>
					</div>
					<div class="contacts__map ya_map" id="contacts_map1" data-center="50.590376, 36.626362" data-zoom="17">

					</div>

					<div class="contacts__requisites">
						<h2 class="contacts__requisites-title">
							Реквизиты компании
						</h2>

						<span class="contacts__requisites-city">
							г. Белгород
						</span>

						<div class="contacts__requisites_container">
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Полное наименование:
								</span>
								<span class="contacts__requisites-text">
									Общество с ограниченной ответственностью “АКС”
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Сокращенное <br>наименование:
								</span>
								<span class="contacts__requisites-text">
									ООО «АКС»
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									ИНН/ КПП:
								</span>
								<span class="contacts__requisites-text">
									3123355376/312301001
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Юридический адрес:
								</span>
								<span class="contacts__requisites-text">
									308000, г. Белгород, Корочанская, 132-А, оф.&nbsp;13
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Фактический адрес:
								</span>
								<span class="contacts__requisites-text">
									308000, г. Белгород, Корочанская, 132-А, оф.&nbsp;13
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Телефон, факс:
								</span>
								<a href="tel:+74722777152" class="contacts__requisites-text">
									+7 (4722) 77-71-52
								</a>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Электронная почта:
								</span>
								<a href="" class="contacts__requisites-text contacts__email">
									acs.31@yandex.ru
								</a>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									БИК:
								</span>
								<span class="contacts__requisites-text">
									041403633
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Расчетный счет:
								</span>
								<span class="contacts__requisites-text">
									40702810207000000250
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Корреспондентский счет:
								</span>
								<span class="contacts__requisites-text">
									30101810100000000633
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Наименование банка:
								</span>
								<span class="contacts__requisites-text">
									Отделение №8592 Сбербанка России г. Белгород
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Директор:
								</span>
								<span class="contacts__requisites-text">
									Дерюгин Андрей Федорович действующий на основании Устава
								</span>
							</div>
						</div>
					</div>


				</div>
				<div class="contacts__col">
					<div class="contacts__info">
						<span class="contacts__city-name">
							г. Курск
						</span>

						<div class="contacts__company-info-item company-info">
							<span class="company-info__legend">
								Режим работы
							</span>
							<span class="company-info__text">
								Пн.–пт.: с 8:00 до 18:00
							</span>
						</div>
						<div class="contacts__company-info-item company-info">
							<span class="company-info__legend">
								Телефон:
							</span>
							<a href="tel:+74722777152" class="company-info__text">
								+7 (4712) 34-30-09
							</a>
						</div>
						<div class="contacts__company-info-item company-info">
							<span class="company-info__legend">
								E-mail:
							</span>
							<a href="" class="company-info__text contacts__email">
								acs46@yandex.ru
							</a>
						</div>
						<adress class="contacts__company-info-item company-info company-info__adress">
							<span class="company-info__legend">
								Адреса:
							</span>
							<span class="company-info__text">
								г. Курск, ул. Республиканская, д. 1Б, лит. А, оф. 44
							</span>
						</adress>
					</div>

					<div class="contacts__map ya_map" id="contacts_map2" data-center="51.742576, 36.235424" data-zoom="17">

					</div>
					<div class="contacts__requisites">
						<h2 class="contacts__requisites-title">
							Реквизиты компании
						</h2>

						<span class="contacts__requisites-city">
							г. Курск
						</span>

						<div class="contacts__requisites_container">
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Полное наименование:
								</span>
								<span class="contacts__requisites-text">
									Общество с ограниченной ответственностью “АКС”
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Сокращенное <br>наименование:
								</span>
								<span class="contacts__requisites-text">
									ООО «АКС»
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									ИНН/ КПП:
								</span>
								<span class="contacts__requisites-text">
									3123355376/312301001
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Юридический адрес:
								</span>
								<span class="contacts__requisites-text">
									308000, г. Белгород, Корочанская, 132-А, оф.&nbsp;13
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Фактический адрес:
								</span>
								<span class="contacts__requisites-text">
									305022, г. Курск, ул. Республиканская, д. 1Б, лит. А, оф.&nbsp;44
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Телефон, факс:
								</span>
								<a href="tel:+74722777152" class="contacts__requisites-text">
									+7 (4712) 34-30-09
								</a>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Электронная почта:
								</span>
								<a href="" class="contacts__requisites-text contacts__email">
									acs46@yandex.ru
								</a>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									БИК:
								</span>
								<span class="contacts__requisites-text">
									044525593
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Расчетный счет:
								</span>
								<span class="contacts__requisites-text">
									40702810702970002114
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Корреспондентский счет:
								</span>
								<span class="contacts__requisites-text">
									30101810200000000593
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Наименование банка:
								</span>
								<span class="contacts__requisites-text">
									АО «АЛЬФА-БАНК» г. Москва
								</span>
							</div>
							<div class="contacts__requisites-item">
								<span class="contacts__requisites-legend">
									Директор:
								</span>
								<span class="contacts__requisites-text">
									Дерюгин Андрей Федорович действующий на основании Устава
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>

<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>

