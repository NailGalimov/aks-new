<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>

<section class="section hero_section hero_section--page --page2">
	<div class="hero_section__img hero_section__img--page">
		<img src="../assets/images/pages/page_1/hero_img_page_2.png" alt="Запорная арматура">
	</div>

	<div class="site-container">
		<div class="main-title main-title--page">
			<h1>
				Комплексные поставки запорной арматуры <br> <span>во все регионы России</span>
			</h1>

			<ul class="hero_section__list">
				<li class="hero_section__item">Экспресс-доставка – от 1 дня</li>
				<li class="hero_section__item">Комплектация строительного объекта под ключ </li>
				<li class="hero_section__item">Сертифицированная продукция от сотни мировых производителей</li>
			</ul>
		</div>

		<div class="btn_wrap btn_wrap--page">
			<button class="button" data-popup="consultation">Получить консультацию</button>
			<p>Получите бесплатную консультацию инженера по подбору продукции для пожаротушения</p>
		</div>
	</div>
</section>

<section class="section page--section-two --page2-section-two js--secTwo">
	<div class="page--section-two__img_helicopter js--secTwoImage">
		<img src="../assets/images/pages/page_1/helicopter.png" alt="Вертолет">
	</div>
	<div class="page--section-two__img">
		<img src="../assets/images/pages/page_1/paris.png" alt="Башня">
	</div>
	<div class="page--section-two__img_sky js--secTwoImage">
		<img src="../assets/images/pages/page_1/sky.png" alt="Облака">
	</div>
	<div class="site-container">
		<div class="main-title">
			<h1>
				Из нашей запорной арматуры <span>можно отстроить Эйфелеву башню</span>
			</h1>
		</div>

		<p class="page--section-two__paragraph">
			За 11 лет работы мы поставили тысячам клиентов по всей России 10 000 тонн запорной арматуры.
			Если собрать все проданные партии, можно заново отстроить Эйфелеву башню в Париже.
		</p>
	</div>
</section>

<section class="section reasons --page2">

	<div class="positioned_block"></div>

	<div class="positioned_block2">
		<div class="arrows_red"></div>
	</div>

	<div class="anim_arrows"></div>

	<div class="site-container">

		<div class="main-title">
			<h2>
				Почему нас выбирают директоры,
				<span>руководители проектов и снабженцы</span>
			</h2>
		</div>

		<div class="feautures__inner" style="background-image: url(../assets/images/feautures_bg.jpg)">

			<div class="feautures__list">
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="300">300</span> <span class="text_m">поставщиков</span></h3>
					<p>благодаря дилерским соглашениям продукция дешевле рынка – от 5% до 30%</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="12000">12 000</span><span class="text_m">объектов</span></h3>
					<p>укомплектовали под ключ: от квартиры до микрорайона и строительной площадки</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="80">80</span> <span class="color-red" >+</span><span class="text_m">сотрудников</span></h3>
					<p>площадь собственного склада мощностью 9 600 палетомест для хранения продукции</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="5000">5000</span> <span class="text_m">единиц</span></h3>
					<p>сертифицированного оборудования в каталоге компании </p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="11">11</span><span class="color-red"> лет</span> <span class="text_m">на рынке</span></h3>
					<p>благодаря огромному опыту работаем быстрее конкурентов и гарантируем результат</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="100">100</span> <span class="text_m">производителей</span></h3>
					<p>поставляем продукцию напрямую с отечественных и зарубежных заводов</p>
				</div>
			</div>

		</div>

	</div>
</section>

<section class="section worlwide worlwide--page --page2">

	<div class="map">
		<?php include $_SERVER['DOCUMENT_ROOT']."../app/include/pages/map_page.php";?>
	</div>

	<div class="worlwide--page__image">
		<img src="../assets/images/pages/page_1/worlwide--page2.png" alt="Оборудование пожарной безопасности">
	</div>

	<div class="site-container">
		<div class="main-title">
			<h2>Федеральный поставщик <span>запорной арматуры</span></h2>
		</div>


		<div class="worlwide__text worlwide--page__text">
			<div class="worlwide__text_img">
				<img src="../assets/images/moon.png" alt="Луна">
			</div>

			<p>
				<strong>Компания «АрмКомплексСнаб»</strong> занимается оптовыми поставками запорно-регулирующей,
				сантехнической арматуры для комплектации строительных объектов по всей России.
				Представляет продукцию ведущих мировых брендов.
			</p>

			<p>
				На нашем сайте можно заказать и недорого приобрести все виды запорных элементов
				и приборов для установки на трубопроводах
				для любой среды: воды и пара, газа, нефти
				и воздуха. Все разновидности устройств, которые предназначены для перекрытия труб, выполнены из качественных материалов.
				Продукция имеет сертификаты качества и высоко ценится во всем мире.
			</p>
		</div>
	</div>
</section>

<section class="section page_catalog page_catalog--page2">
	<div class="site-container">
		<div class="main-title rest">
			<h2>
				Каталог запорной арматуры: 1 000+ <span>товаров в наличии <br> и под заказ</span>
			</h2>
			<p>
				В нашем интернет-магазине
				с гарантией – от 1 года до 5 лет
			</p>
		</div>

		<?
			$product_arr = ["Задвижки", "Вентили (клапаны)", "Краны шаровые", "Фитинги"];
			$num = 0;
		?>

		<div class="main_list">

			<? foreach ($product_arr as $val) {?>
				<? $num++; ?>
				<div class="main_list__col">
					<a href="cart.php" class="main_list__item" style="background-image: url(../assets/images/pages/page_1/cart_img<? echo $num; ?>.jpg)">
						<div class="main_list__item_text">
							<p class="main_list__text"><? echo $val; ?></p>
							<p class="main_list__text--hidden">
								Односедельные и двухседельные,
								нормально открытые и закрытые
							</p>

						</div>

						<div class="our_products__item_btn item_btn">
							<span class="item_btn--hover" data-text-2="Подоробнее" data-text="ОТ 17 000 РУБЛЕЙ" >Подробнее</span>
							<div class="item_btn__icon">
								<span class="icon-right-arrow arrow-right"></span>
								<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
									<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
								</svg>
							</div>
						</div>
					</a>
				</div>
			<? } ?>
		</div>
	</div>
</section>

<section class="section clients">

	<div class="main-title">
		<h2>
			<span>Продукция производства </span>
			мировых брендов
		</h2>
	</div>

	<div class="clients__wrapper">
		<?php include $_SERVER['DOCUMENT_ROOT']."../app/include/pages/clients.php";?>

		<a href="" class="button-arrow clients__btn">
			<span>Посмотреть все</span>
			<div class="icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
					<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"></path>
				</svg>
			</div>
		</a>
	</div>

</section>


<section class="section portfolio portfolio--page">
	<div class="portfolio__bottom_line"></div>
	<div class="portfolio__img">
		<img src="../assets/images/porfolio_map.png" alt="">
	</div>


	<div class="site-container">
		<div class="main-title rest">
			<h2><span>Реализованные</span> под ключ проекты</h2>
		</div>

		<div class="custom-arrows portfolio__arrow-container"></div>


		<?php include $_SERVER['DOCUMENT_ROOT']."../app/include/pages/portfolio-slider.php";?>

		<div class="text-center">
			<a class="portfolio__see_all" href="projects.php">Посмотреть все объекты</a>
		</div>

	</div>
</section>

<section class="section footer_section footer_section--page">

	<div class="footer_section__img footer_section--page__img">
		<img src="../assets/images/page-footer-img.png" alt="">
	</div>

	<div class="site-container">
		<div class="footer_section__row">
			<div class="footer_section__col">
				<div class="footer_section__item">
					<div class="main-title">
						<h2>Получите расчет стоимости <span>поставки</span></h2>
					</div>

					<ul>
						<li><strong>Подберем оптимальный вариант оборудования</strong> для комплектации вашего объекта.</li>
						<li><strong>Предложим 3 варианта комплектации</strong> объекта в рамках бюджета.</li>
					</ul>
				</div>
			</div>
			<div class="footer_section__col">
				<div class="footer_section__item">
					<div class="form_custom">
						<form action="" class="formValidate validate" name="main-form">
							<h5>Оставьте заявку,</h5>
							<p>мы свяжемся с вами по телефону, поможем вам выбрать трубы
								с оптимальными характеристиками
								и рассчитаем стоимость покупки
							</p>

							<div class="input_wrapper">
								<div class="input_container">
									<input type="text" placeholder="Ваше имя" name="name" class="required" data-mask="fio">
								</div>
								<div class="input_container">
									<input type="text" placeholder="Ваш номер" name="tell" class="required" data-mask="phone">
								</div>
								<div class="input_container">
									<input type="text" placeholder="Ваша почта" name="e-mail" class="required" data-mask="email">
								</div>
							</div>

							<div class="text-center">
								<button class="button" type="submit">Оставить заявку</button>
							</div>

							<label class="form-agreement">
								<input class="form-agreement__input required" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
								<span class="form-agreement__text">
									<span class="form-agreement__check"></span>
									Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
								</span>
							</label>

						</form>
					</div>
				</div>
			</div>
		</div>





	</div>
</section>



<? include $_SERVER['DOCUMENT_ROOT'].'../app/html/footer.php' ?>
