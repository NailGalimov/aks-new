<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
<div class="site-container">
    <? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
</div>

<section class="section solution-page">
	<div class="solution-page__image">
        <img src="../assets/images/pages/solutions/hero-img.png" alt="Небоскреб">
    </div>

	<div class="site-container">
		<div class="solution-page__title main-title">
			<h1>
				Комплектуем инвестпроекты оборудованием <span>для инженерных сетей под ключ</span>
			</h1>

			<p>
				10&nbsp;комплексных решений для строительной индустрии
			</p>

			<ul class="hero_section__list">
				<li class="hero_section__item">
					Экспресс-доставка до&nbsp;объекта по&nbsp;России и&nbsp;СНГ: собственным корпоративным грузовым автопарком или транспортными компаниями
				</li>
			</ul>
		</div>

		<div class="btn_wrap">
			<button class="button" data-popup="price">Рассчитать стоимость</button>
			<p>Получите расчет стоимости комплектации вашего объекта</p>
		</div>
	</div>
</section>

<section class="section full-solution">
	<div class="site-container">
		<div class="full-solution__title">
			<h2>
				Полная комплектация <span>объектов инженерными системами от&nbsp;100 известных производителей</span>
			</h2>
		</div>


		<div class="full-solution__container">
			<?
				$arr = [
					"Запорная арматура" => ["Задвижки", "Вентили (клапаны)", "Краны шаровые", "Фитинги"],
					"Приборы учета и КИПиА" => ["Счетчики газа", "Счетчики воды", "Теплосчетчики", "Манометры"],
					"Газовое оборудование" => ["Трубы полиэтиленовые ГАЗ", "Газорегуляторное оборудование", "Краны шаровые", "Приборы учета газа"],
					"Крепежные системы" => ["Анкерный крепеж", "Метрический крепеж", "Саморезы", "Болты"],
					"Насосное оборудование" => ["Канализационные насосные станции", "Насосные станции водоснабжения и пожаротушения", "Насосы для агрессивных жидкостей"],
					"Котельное и теплообменное оборудование" => ["Блочные котельные установки", "Кожухотрубные теплообменники", "Котельное оборудование", "Вспомогательное оборудование"],
					"Теплоизоляционное оборудование" => ["Трубы полиэтиленовые ГАЗ", "Газорегуляторное оборудование", "Краны шаровые", "Приборы учета газа"],
					"Емкостное оборудование" => ["Жироуловители", "Локальные очистные сооружения", "Емкости	из стеклопластика"],
					"Пожарное оборудование" => ["Огнетушители", "Регуляторы давления", "Пожарные гидранты", "Спринклерные системы"],
					"Трубы и комплектующие" => ["Трубы гофрированные", "Трубы ПВХ", "Фитинги", "Детали трубопровода"]
				];
				$num = 0;
			?>

			<? foreach ($arr as $key => $value) {?>
				<? $num++; ?>
				<article class="full-solution__item <? if($num === 1 || $num == 4 || $num == 5 || $num == 8 || $num == 9) echo "grey-bg" ?> ">

					<div class="full-solution__item-img">
						<img src="../assets/images/pages/solutions/item-img<? echo $num; ?>.png" alt="<? echo $key; ?>">
					</div>

					<h3 class="full-solution__item-title">
						<? echo $key; ?>
					</h3>

					<ul class="full-solution__item-list">
						<? foreach( $value as $val) {?>
							<li class="full-solution__item-text">
								<? echo $val; ?>
							</li>
						<? } ?>
					</ul>

					<a href="page_<? echo $num; ?>" class="button-arrow">
						<span>Подробнее</span>
						<div class="icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
								<path id="icon-arrow"
									d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z"
									transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f"
									stroke-width="2"></path>
							</svg>
						</div>
					</a>

				</article>
			<? } ?>
		</div>
	</div>
</section>


<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php' ?>
