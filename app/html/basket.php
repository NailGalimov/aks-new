<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
	<div class="site-container">
		<? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
	</div>

	<section class="section basket">
		<div class="site-container">
			<h1 class="basket__title">
				моя корзина
			</h1>
			<!-- Если пустая корзина -->
			<? if ($_GET['empty'] == "Y") {?>
				<div class="basket__info">
					<span class="basket__legend">
						В корзине нет товаров
					</span>
					<p class="basket__text">
						Найдите то, что вам нужно в каталоге или при помощи <span>поиска</span>
					</p>

					<a href="catalog.php" class="button-arrow basket__button">
						<span>Вернуться к покупкам</span>
						<div class="icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
								<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"></path>
							</svg>
						</div>
					</a>
				</div>
			<!-- иначе товары -->
			<? } else {?>
				<div class="basket__row">
					<div class="basket__col">
						<div class="basket__container order-cart">
							<div class="order-cart__head">
								<span class="order-cart__quantity order-cart--span-style">
									Позиций в вашей корзине: (99)
								</span>
								<span class="order-cart__price order-cart--span-style">
									Цена
								</span>
								<span class="order-cart__quant order-cart--span-style">
									Количесиво
								</span>
								<span class="order-cart__summ order-cart--span-style">
									Сумма
								</span>
							</div>
							<ul class="order-cart__body basket-cart__list">
								<? for($i=0; $i<5; $i++) {?>
								<li class="basket-cart__item">
									<span class="icon-cross"></span>
									<span class="basket-cart__mobile-naming">Наименование</span>
									<a href="" class="basket-cart__info">
										<img src="../assets/images/mini-cart-img1.jpg" alt="Задвижка стальная">
										<div class="basket-cart__name-wrap">
											<span class="basket-cart__name">
												Задвижка стальная 30с41нж Ру16  (ЗКЛ-2-16) МЗТА
											</span>
											<span class="basket-cart__in-stock">
												в наличии
											</span>
										</div>
									</a>
									<div class="basket-cart__price">
										<span class="basket-cart__mobile-naming">Цена</span>
										99 000
										<span class="icon-ruble"></span>
									</div>
									<div class="basket-cart__quant">
										<span class="basket-cart__mobile-naming">Количество</span>
										<div class="quant" data-id="<?echo $i;?>">
											<span class="quant__minus quant__btn quant__btn--minus --disabled"><span class="icon-remove"></span></span>
											<span class="quant__input--wrap">
												<input type="text" class="quant__input" value="1" maxlength="3">
												<span>шт</span>
											</span>
											<span class="quant__plus quant__btn quant__btn--plus"><span class="icon-plus"></span></span>
										</div>
									</div>
									<div class="basket-cart__summ">
										<span class="basket-cart__mobile-naming">Сумма</span>
										99 000
										<span class="icon-ruble"></span>
									</div>
								</li>
								<?}?>
							</ul>
							<button class="order-cart__clear-cart button-reset">
								Очистить всю корзину
							</button>
						</div>
					</div>
					<div class="basket__col">
						<div class="order-cart__ordering ordering">
							<div class="ordering__top">
								<div class="ordering__row">
									<span class="ordering__legeng">
										Товары
									</span>
									<div class="ordering__price">
										110 000
										<span class="icon-ruble"></span>
									</div>
								</div>
								<div class="ordering__row">
									<span class="ordering__legeng">
										Скидка на товар
									</span>
									<div class="ordering__price ordering__price--discount">
										-1000
									</div>
								</div>
								<div class="ordering__row">
									<span class="ordering__legeng">
										Итого:
									</span>
									<div class="ordering__price ordering__price--final-price">
										109 000
										<span class="icon-ruble"></span>
									</div>
								</div>
							</div>
							<div class="ordering__bottom">
								<div class="ordering__discount">
									<input class="ordering__input" type="text" placeholder="Промокод на скидку">
									<button class="button-reset ordering__bottom-button">
										Применить
									</button>
								</div>
								<div class="ordering__delivery">
									<span class="ordering__delivery-title">
										Где и как вы хотите получить заказ?
									</span>
									<div class="ordering__delivery-row">
										<label class="ordering__delivery-item">
											<input class="ordering__delivery-radio" type="radio" id="1" name="delivery" value="На складе">
											<span class="ordering__delivery-box"></span>
											<span class="ordering__delivery-name">На складе</span>
											<span class="ordering__delivery-info">Сегодня, бесплатно</span>
										</label>
										<label class="ordering__delivery-item">
											<input class="ordering__delivery-radio" type="radio" id="2" name="delivery" value="На складе">
											<span class="ordering__delivery-box"></span>
											<span class="ordering__delivery-name">Оформить доставку</span>
											<span class="ordering__delivery-info">Завтра и позже</span>
										</label>
									</div>
								</div>
								<a href="ordering.php" class="button-arrow ordering__button">
									<span>Перейти к оформлению</span>
									<div class="icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
											<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"></path>
										</svg>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			<? }?>
		</div>
	</section>


<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
