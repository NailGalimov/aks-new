<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
	<div class="site-container">
		<? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
	</div>

	<section class="section search-page">
		<div class="site-container">
			<h1 class="search-page__title">
				Поиск по каталогу
			</h1>
			<div class="search-page__content">
				<form class="search-page__form">
					<div class="search-page__form-inner">
						<div class="search-page__input-wrap">
							<input type="text" class="search-page__input" name="search" placeholder="Поиск по каталогу">
						</div>
						<button class="search-page__button button-reset"><span class="icon-search"></span></button>
					</div>
				</form>

				<div class="search-page__catalog">
					<?
						$product_arr_first = ["Задвижка стальная 30с541нж Ру16 МЗТА ", "Задвижка стальная", "Отводы ГОСТ 17375-2001", "Краны латунные шаровые 11б27п (газ)", "Труба армированная, зачистная – PN 25", "Задвижка стальная 30с41нж Ру16 (ЗКЛ-2-16)", "Краны шаровые для жидкости фланцевого присоединения", "Задвижка стальная 30с41нж Ру16 (ЗКЛ-2-16) МЗТА"];
						$num_first = 0;
					?>

					<div class="our_products__list main_list">

						<? foreach ($product_arr_first as $val) {?>
							<? $num_first++; ?>
							<div class="main_list__col">
								<a href="" class="main_list__item" style="background-image: url(../assets/images/product_img<? echo $num_first; ?>.jpg)">
									<div class="main_list__item_text">
										<p class="main_list__text"><? echo $val; ?></p>
										<p class="main_list__text--hidden">
											Односедельные и двухседельные,
											нормально открытые и закрытые
										</p>

									</div>
									<div class="our_products__item_btn item_btn">
										<span class="item_btn--hover" data-text-2="Подоробнее" data-text="ОТ 17 000 РУБЛЕЙ" >Подробнее</span>
										<div class="item_btn__icon">
											<span class="icon-right-arrow arrow-right"></span>
											<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
												<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
											</svg>
										</div>
									</div>
								</a>
							</div>
						<? } ?>
					</div>
				</div>

				<div class="search-page__pagination">
					<? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/pagination.php' ?>
				</div>
			</div>
		</div>

	</section>


<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
