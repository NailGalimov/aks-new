<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
<div class="site-container">
    <? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
</div>

<section class="section loyalty">
    <div class="loyalty__image">
        <img src="../assets/images/pages/loyalty/hero-img.png"
            alt="Система лояльности для юридических и физических лиц">
    </div>

    <div class="site-container">
        <div class="loyalty__title main-title">
            <h1>
                Система лояльности <span>Для юридических и&nbsp;физических лиц</span>
            </h1>
        </div>

        <ul class="hero_section__list">
            <li class="hero_section__item">
                Помощь в&nbsp;проектировании и&nbsp;комплектации
            </li>
            <li class="hero_section__item">
                Сопровождение проекта личным менеджером от&nbsp;заявки до&nbsp;поставки на&nbsp;объект
            </li>
            <li class="hero_section__item">
                Оптовые цены от&nbsp;производителя. Отсрочка платежа
            </li>
        </ul>

        <div class="btn_wrap">
            <button class="button" data-popup="spez">Получить спецпредложение</button>
            <p>Оставьте заявку и получите выгодные <br> условия сотрудничества</p>
        </div>

    </div>
</section>
<section class="section five-steps">
    <div class="site-container">
        <div class="five-steps__title main-title">
            <h2>
                Получите поставку оборудования <span>за&nbsp;5&nbsp;шагов</span>
            </h2>
        </div>
        <ul class="five-steps__list">
            <li class="five-steps__item">
                <span class="five-steps__legend">
                    Оставьте заявку любым удобным способом:
                </span>

                <span class="five-steps__legend">
                    На сайте:
                </span>

                <button class="button five-steps__button" data-popup="consultation">Оставить заявку</button>

                <span class="five-steps__legend">
                    Позвоните нам по телефону:
                </span>

                <a class="five-steps__tel" href="tel:+74722777152">+7 (4722) 77-71-52</a>

                <span class="five-steps__legend">
                    Напишите на E-mail:
                </span>

                <a class="five-steps__email" href="#">acs.31@yandex.ru</a>
            </li>

            <li class="five-steps__item">
                <div class="five-steps__img">
                    <img src="../assets/images/pages/loyalty/cart-img1.png" alt="Краны">
                </div>

                <p class="five-steps__text">
                    Получите подбор оборудования из&nbsp;20&nbsp;000 наименований и&nbsp;выгодный расчет стоимости.
                    Предложим самый оптимальный вариант из&nbsp;доступных на&nbsp;рынке СНГ по&nbsp;цене, качеству
                    и&nbsp;срокам поставки.
                </p>
            </li>
            <li class="five-steps__item">
                <div class="five-steps__img">
                    <img src="../assets/images/pages/loyalty/cart-img2.png" alt="Краны">
                </div>

                <p class="five-steps__text">
                    Заключим договор и&nbsp;поставим купленные товары в&nbsp;кратчайшие сроки благодаря многолетнему
                    опыту и&nbsp;продуманной логистике.
                </p>
            </li>
            <li class="five-steps__item">
                <div class="five-steps__img">
                    <img src="../assets/images/pages/loyalty/cart-img3.png" alt="Краны">
                </div>

                <p class="five-steps__text">
                    Получите гарантийное сопровождение оборудования. Оперативно решаем вопросы с&nbsp;изготовителем,
                    рекламациями и&nbsp;срочной поставкой.
                </p>
            </li>
            <li class="five-steps__item">
                <div class="five-steps__img">
                    <img src="../assets/images/pages/loyalty/cart-img4.png" alt="Краны">
                </div>

                <p class="five-steps__text">
                    Поставим все необходимые запасные части и&nbsp;комплектующие. Готовы работать на&nbsp;долгосрочную
                    перспективу.
                </p>
            </li>
        </ul>
    </div>
</section>

<section class="section loyalty-clients">
    <div class="site-container">
        <div class="systems__list loyalty-clients__list">
            <div class="systems__col">
                <div class="systems__item loyalty-clients__item">
                    <h3>Система лояльности <span>для юр. лиц</span></h3>

                    <ul>
                        <li>Оптовые цены от&nbsp;производителя в&nbsp;зависимости от&nbsp;объемов поставки. Скидки
                            до&nbsp;35&nbsp;%.</li>
                        <li>Гибкая система товарного кредита.</li>
                        <li>Индивидуальные условия экспресс-доставки курьерскими службами и&nbsp;собственным автопарком.
                        </li>
                        <li>Сопровождение проекта личным менеджером. Гарантия оперативного решения всех рабочих вопросов
                            ежедневно без выходных.</li>
                    </ul>

                    <div class="systems__item_arrows"></div>
                    <div class="systems__item_img loyalty-clients__item-img">
                        <img src="../assets/images/system_img1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="systems__col">
                <div class="systems__item loyalty-clients__item">
                    <h3>Система лояльности <span>для частных лиц</span></h3>
                    <ul>
                        <li>Помощь в&nbsp;проектировании и&nbsp;комплектации.</li>
                        <li>Предложения на&nbsp;любой бюджет и&nbsp;под любую задачу. Поможем заказчику выявить
                            и&nbsp;избежать избыточных проектных решений.</li>
                        <li>Срочные поставки до&nbsp;объекта в&nbsp;день обращения.</li>
                        <li>Товарный кредит на&nbsp;срок до&nbsp;60&nbsp;дней без изменения коммерческих условий.</li>
                        <li>Еженедельные акции на&nbsp;группы товаров.</li>
                        <li>Накопительная система скидок.</li>
                        <li>Ежемесячный розыгрыш подарков.</li>
                        <li>Статус VIP-клиента при годовой покупке от&nbsp;500&nbsp;000&nbsp;рублей. Максимальная
                            скидка, персональный менеджер.</li>
                    </ul>

                    <div class="systems__item_arrows"></div>
                    <div class="systems__item_img loyalty-clients__item-img">
                        <img src="../assets/images/system_img2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section footer_section footer_section--page loyalty-form">

    <div class="loyalty-form__img">
        <img src="../assets/images/pages/loyalty/gear.png" alt="Шестеренки">
    </div>

    <div class="site-container">
        <div class="footer_section__row">
            <div class="footer_section__col">
                <div class="footer_section__item">
                    <div class="main-title">
                        <h2>Получите <span>спецусловия сотрудничества</span></h2>
                    </div>

                    <ul>
                        <li><strong>Подберем выгодное коммерческое предложение</strong> под вашу задачу.
                        </li>
                        <li><strong>Предложим 3 варианта комплектации</strong> объекта в рамках бюджета.</li>
                        <li>
                            <strong>Рассчитаем проект с учетом отсрочки</strong> платежа – по договоренности.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer_section__col">
                <div class="footer_section__item">
                    <div class="form_custom">
                        <form action="" class="formValidate validate" name="main-form" novalidate="novalidate">
                            <h5>Оставьте заявку,</h5>
                            <p>
								и&nbsp;мы&nbsp;перезвоним вам и&nbsp;сделаем расчет стоимости комплектации вашего объекта
                            </p>

                            <div class="input_wrapper">
                                <div class="input_container">
                                    <input type="text" placeholder="Ваше имя" name="name" class="required"
                                        data-mask="fio">
                                </div>
                                <div class="input_container">
                                    <input type="text" placeholder="Ваш номер" name="tell" class="required"
                                        data-mask="phone">
                                </div>
                                <div class="input_container">
                                    <input type="text" placeholder="Ваша почта" name="e-mail" class="required"
                                        data-mask="email">
                                </div>
                            </div>

                            <div class="text-center">
                                <button class="button" type="submit">Оставить заявку</button>
                            </div>

							<label class="form-agreement">
								<input class="form-agreement__input required" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
								<span class="form-agreement__text">
									<span class="form-agreement__check"></span>
									Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
								</span>
							</label>

                        </form>
                    </div>
                </div>
            </div>
        </div>





    </div>
</section>

<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
