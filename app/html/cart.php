<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
	<div class="site-container">
		<? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
	</div>

	<section class="section cart">
		<div class="site-container">
			<div class="cart__top">
				<div class="cart__images">
					<div class="cart__images--big">
						<? for($i=0; $i<5; $i++) {?>
							<img src="../assets/images/pages/cart/cart-img1.jpg" alt="Задвижка стальная">
						<?}?>
					</div>
					<div class="cart__images--small">
						<? for($i=0; $i<5; $i++) {?>
							<div class="cart__images--small-wrap">
								<img src="../assets/images/pages/cart/cart-img1.jpg" alt="Задвижка стальная">
							</div>
						<?}?>
					</div>
				</div>
				<div class="cart__info">
					<h3 class="cart__name">
						Задвижка стальная 30с41нж Ру16 (ЗКЛ-2-16) МЗТА
					</h3>
					<span class="cart__in-stock">
						В наличии
					</span>
					<span class="cart__price">
						99 000 <span class="icon-ruble"></span>
					</span>

					<div class="cart__btn-group">
						<div class="cart__quant quant" data-id="1">
							<span class="quant__minus quant__btn quant__btn--minus --disabled"><span class="icon-remove"></span></span>
							<span class="quant__input--wrap">
								<input type="text" class="quant__input" value="1" maxlength="3">
								<span>шт</span>
							</span>
							<span class="quant__plus quant__btn quant__btn--plus"><span class="icon-plus"></span></span>
						</div>
						<button class="button cart__button">
							В корзину
							<span class="icon-basket"></span>
						</button>
					</div>
					<p class="cart__help">
						При заказе необходимо указать: обозначение изделия, номинальный диаметр (DN), параметры рабочей среды, необходимость комплектации ответными фланцами (комплект монтажных частей).
					</p>
				</div>
			</div>
			<div class="cart__bottom">
				<div class="cart__tabs tabs">
					<ul class="cart__nav">
						<li class="cart__nav-item active" data-show="1">
							Описание
						</li>
						<li class="cart__nav-item" data-show="2">
							Технические характеристики
						</li>
						<li class="cart__nav-item" data-show="3">
							Как купить <span>?</span>
						</li>
					</ul>

					<article class="cart__content active" data-item="1">
						<p class="cart__content-info">
							Уплотнительные поверхности корпуса и&nbsp;клина наплавлены коррозионностойкой сталью, что позволяет длительно эксплуатировать задвижки с&nbsp;заданной герметичностью.
						</p>

						<div class="cart__content-row">
							<div class="cart__content-col">
								<span class="cart__content-legend">
									Материалы основных деталей:
								</span>

								<ul class="cart__content-list">
									<li class="cart__content-item">
										Корпус, крышка, маховик, клин&nbsp;&mdash; сталь 25Л
									</li>
									<li class="cart__content-item">
										Шпиндель&nbsp;&mdash; 20Х13Л
									</li>
									<li class="cart__content-item">
										Втулка резьбовая&nbsp;&mdash; БрАж9-4
									</li>
									<li class="cart__content-item">
										Материал наплавки затвора: коррозионностойкая сталь с&nbsp;содержанием хрома не&nbsp;менее&nbsp;12%
									</li>
									<li class="cart__content-item">
										Набивка сальника&nbsp;&mdash; АП&nbsp;31
									</li>
									<li class="cart__content-item">
										Гарантийная наработка&nbsp;&mdash; 450 циклов в&nbsp;пределах гарантийного срока эксплуатации
									</li>
								</ul>
							</div>
							<div class="cart__content-col">
								<span class="cart__content-legend">
									Применение
								</span>
								<p class="cart__content-text">
									Предназначена для установки в&nbsp;качестве запорного устройства на&nbsp;трубопроводах по&nbsp;транспортировке воды, пара, масел, нефти, жидких неагрессивных нефтепродуктов, неагрессивных жидких и&nbsp;газообразных сред, по&nbsp;отношению к&nbsp;которым материалы, применяемые в&nbsp;задвиж
								</p>
							</div>

						</div>
					</article>
					<article class="cart__content" data-item="2">
						<p class="cart__content-info">
							Уплотнительные поверхности корпуса и&nbsp;клина наплавлены коррозионностойкой сталью, что позволяет длительно эксплуатировать задвижки с&nbsp;заданной герметичностью.
						</p>

						<div class="cart__content-row">
							<div class="cart__content-col">
								<span class="cart__content-legend">
									Материалы основных деталей:
								</span>

								<ul class="cart__content-list">
									<li class="cart__content-item">
										Корпус, крышка, маховик, клин&nbsp;&mdash; сталь 25Л
									</li>
									<li class="cart__content-item">
										Шпиндель&nbsp;&mdash; 20Х13Л
									</li>
									<li class="cart__content-item">
										Втулка резьбовая&nbsp;&mdash; БрАж9-4
									</li>
								</ul>
							</div>
							<div class="cart__content-col">
								<span class="cart__content-legend">
									Применение
								</span>
								<p class="cart__content-text">
									Предназначена для установки в&nbsp;качестве запорного устройства на&nbsp;трубопроводах по&nbsp;транспортировке воды, пара, масел, нефти, жидких неагрессивных нефтепродуктов, неагрессивных жидких и&nbsp;газообразных сред, по&nbsp;отношению к&nbsp;которым материалы, применяемые в&nbsp;задвиж
								</p>
							</div>

						</div>
					</article>
					<article class="cart__content" data-item="3">
						<p class="cart__content-info">
							Уплотнительные поверхности корпуса и&nbsp;клина наплавлены коррозионностойкой сталью, что позволяет длительно эксплуатировать задвижки с&nbsp;заданной герметичностью.
						</p>

						<div class="cart__content-row">
							<div class="cart__content-col">
								<span class="cart__content-legend">
									Материалы основных деталей:
								</span>

								<ul class="cart__content-list">
									<li class="cart__content-item">
										Корпус, крышка, маховик, клин&nbsp;&mdash; сталь 25Л
									</li>
									<li class="cart__content-item">
										Шпиндель&nbsp;&mdash; 20Х13Л
									</li>
								</ul>
							</div>
							<div class="cart__content-col">
								<span class="cart__content-legend">
									Применение
								</span>
								<p class="cart__content-text">
									Предназначена для установки в&nbsp;качестве запорного устройства на&nbsp;трубопроводах по&nbsp;транспортировке воды, пара, масел, нефти, жидких неагрессивных нефтепродуктов, неагрессивных жидких и&nbsp;газообразных сред, по&nbsp;отношению к&nbsp;которым материалы, применяемые в&nbsp;задвиж
								</p>
							</div>

						</div>
					</article>
				</div>

				<div class="cart__popular">
					<div class="cart__popular--wrap">
						<h3 class="cart__popular-legend">
							Популярные товары
						</h3>
						<a class="cart__popular-link" href="">Смотреть все</a>
					</div>

					<?
						$arr = ["Приборы учета газа", "Счетчики воды", "Задвижка стальная 30с541нж Ру16 МЗТА", "Насосы общего  применения"];
						$num = 0;
					?>

					<div class="cart__popular-list main_list">

						<? foreach ($arr as $val) {?>
							<? $num++; ?>
							<div class="main_list__col">
								<a href="" class="main_list__item" style="background-image: url(../assets/images/pages/cart/product_img<? echo $num; ?>.jpg)">
									<div class="main_list__item_text">
										<p class="main_list__text"><? echo $val; ?></p>
									</div>

									<div class="our_products__item_btn item_btn">
										<span class="item_btn--hover" data-text-2="Подоробнее" data-text="ОТ 17 000 РУБЛЕЙ" >Подробнее</span>
										<div class="item_btn__icon">
											<span class="icon-right-arrow arrow-right"></span>
											<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
												<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
											</svg>
										</div>
									</div>
								</a>
							</div>
						<? } ?>
					</div>

				</div>


			</div>
		</div>
	</section>

	<section class="section footer_section footer_section--page projects-form">

		<div class="projects-form__img --calc">
			<img src="../assets/images/pages/projects/calc.png" alt="Калькулятор">
		</div>

		<div class="projects-form__img --tube">
			<img src="../assets/images/pages/projects/tube.png" alt="Трубы">
		</div>

		<div class="site-container">
			<div class="footer_section__row">
				<div class="footer_section__col">
					<div class="footer_section__item">
						<div class="main-title">
							<h2>Получите расчет стоимости <span>проекта</span></h2>
						</div>

						<ul>
							<li><strong>Подберем и&nbsp;рассчитаем </strong> необходимое количество продукции под вашу задачу.
							</li>
							<li><strong>Сделаем перерасчет материалов</strong> с&nbsp;использованием бюджетных аналогов.</li>
						</ul>
					</div>
				</div>
				<div class="footer_section__col">
					<div class="footer_section__item">
						<div class="form_custom">
							<form action="" class="formValidate validate" name="main-form" novalidate="novalidate">
								<h5>Оставьте заявку,</h5>
								<p>и&nbsp;мы&nbsp;перезвоним вам и&nbsp;сделаем расчет стоимости комплектации вашего объекта
								</p>

								<div class="input_wrapper">
									<div class="input_container">
										<input type="text" placeholder="Ваше имя" name="name" class="required"
											data-mask="fio">
									</div>
									<div class="input_container">
										<input type="text" placeholder="Ваш номер" name="tell" class="required"
											data-mask="phone">
									</div>
									<div class="input_container">
										<input type="text" placeholder="Ваша почта" name="e-mail" class="required"
											data-mask="email">
									</div>
								</div>

								<div class="text-center">
									<button class="button" type="submit">Оставить заявку</button>
								</div>

								<label class="form-agreement">
									<input class="form-agreement__input required" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
									<span class="form-agreement__text">
										<span class="form-agreement__check"></span>
										Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
									</span>
								</label>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
