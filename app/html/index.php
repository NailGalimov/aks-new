<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>

<section class="section hero_section">
	<div class="hero_section__img">
		<img src="../assets/images/hero_img.png" alt="">
	</div>

	<div class="site-container">
		<div class="main-title">
			<h1>
				Федеральный поставщик Оборудования инженерных сетей:
				<span>укомплектовали 12 000 объектов с 2009 года</span>
			</h1>
		</div>

		<div class="btn_wrap">
			<button class="button" data-popup="consultation">Получить консультацию</button>
			<p>Консультация технического специалиста бесплатно</p>
		</div>
	</div>
</section>

 <section class="section feautures countAnimationTrigger">

	<div class="anim_arrows"></div>
	<div class="positioned_block"></div>
	<div class="positioned_block2">
		<div class="arrows_red"></div>
	</div>

	<div class="site-container">
		<div class="feautures__inner" style="background-image: url(../assets/images/feautures_bg.jpg)">

			<div class="feautures__list">
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="20000">20 000</span><span class="color-red" >+</span></h3>
					<p>сертифицированных</p>
					<p>продуктов в наличии</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="11">11</span> <span class="color-red" >лет</span></h3>
					<p>на российском рынке</p>
					<p>инженерных систем</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="5600">5600</span> <span class="color-red" >м <sup>2</sup></span></h3>
					<p>собственный склад</p>
					<p>для хранения товаров</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="100">100</span><span class="color-red" >+</span></h3>
					<p>партнерских соглашений</p>
					<p>с заводами - производителями</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="80">80</span><span class="color-red" >+</span></h3>
					<p>сотрудников</p>
					<p>в штате компании</p>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="section our_products">
	<div class="our_products__arrows">
	</div>

	<div class="our_products__blocks">
		<div></div>
		<div></div>
	</div>

	<div class="site-container">
		<div class="main-title">
			<h2>20 000 наименований <span>товаров в каталоге</span></h2>
			<p><strong>Сертифицированная продукция от 100</strong></p>
			<p>отечественных и зарубежных производителей</p>
		</div>

		<?
			$product_arr_first = ["Задвижка стальная 30с541нж Ру16 МЗТА ", "Задвижка стальная", "Отводы ГОСТ 17375-2001", "Краны латунные шаровые 11б27п (газ)", "Труба армированная, зачистная – PN 25", "Задвижка стальная 30с41нж Ру16 (ЗКЛ-2-16)", "Краны шаровые для жидкости фланцевого присоединения", "Задвижка стальная 30с41нж Ру16 (ЗКЛ-2-16) МЗТА"];
			$num_first = 0;
		?>

		<div class="our_products__list main_list catalogContainer">

			<? foreach ($product_arr_first as $val) {?>
				<? $num_first++; ?>
				<div class="main_list__col">
					<a href="cart.php" class="main_list__item" style="background-image: url(../assets/images/product_img<? echo $num_first; ?>.jpg)">
						<div class="main_list__item_text">
							<p class="main_list__text"><? echo $val; ?></p>
							<p class="main_list__text--hidden">
								Односедельные и двухседельные,
								нормально открытые и закрытые
							</p>

						</div>

						<div class="our_products__item_btn item_btn">
							<span class="item_btn--hover" data-text-2="Подоробнее" data-text="ОТ 17 000 РУБЛЕЙ" >Подробнее</span>
							<div class="item_btn__icon">
								<span class="icon-right-arrow arrow-right"></span>
								<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
									<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
								</svg>
							</div>
						</div>
					</a>
				</div>
			<? } ?>
		</div>

		<div class="text-center">
			<a href="catalog.php" class="our_products__btn button-arrow load-more-js">
				<span>еще</span>
				<div class="icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
						<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
					</svg>
				</div>
			</a>
		</div>


	</div>
</section>

<section class="section worlwide">

	<div class="map">
		<?php include $_SERVER['DOCUMENT_ROOT']."../app/include/pages/map.php";?>
	</div>

	<div class="site-container">
		<div class="main-title">
			<h2>Компания - интегратор: <span>комплектуем инвестпроекты по всей России</span></h2>
			<p><strong>Снабжаем</strong> юридических лиц, индивидуальных</p>
			<p>предпринимателей, монтажные организации и частных лиц</p>
		</div>


		<div class="worlwide__text">
			<div class="worlwide__text_img">
				<img src="../assets/images/moon.png" alt="">
			</div>

			<p>
				<strong><span>Компания «АрмКомплексСнаб»</span> осуществляет комплексные поставки оборудования <br>
				для инженерных сетей</strong> <br> по всей стране. Благодаря сотне дилерских соглашений и сотрудничеству
				<br> с 300 поставщиками продукция дешевле рынка <br>
				– от 5% до 30%.
			</p>

			<p>
				Компания полностью комплектует объекты любого масштаба: от квартиры до целого микрорайона <br>
				и строительной площадки. Обеспечила реализацию оборудования <br>
				для водоснабжения, отопления и газоснабжения <br>
				на 12 000 объектов в РФ и СНГ.
			</p>
		</div>

		<a href="about.php" class="button-arrow icon-right">
			<span>Подробнее</span>
			<div class="icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
					<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
				</svg>
			</div>
		</a>



	</div>
</section>

<section class="section systems">
	<div class="systems__bg_line"></div>
	<div class="block-wrapper">
		<div class="systems__bloks">
			<div></div>
			<div></div>
		</div>
	</div>

	<div class="site-container">
		<div class="systems__list">
			<div class="systems__col">
				<div class="systems__item">
					<h3>Система лояльности <span>для юр. лиц</span></h3>
					<ul>
						<li>Оптовые цены от производителя</li>
						<li>Индивидуальные условия доставки</li>
						<li>Сопровождение проекта личным менеджером</li>
					</ul>

					<a href="clients.php" class="our_products__btn button-arrow icon-right">
						<span>узнать больше</span>
						<div class="icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
								<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
							</svg>
						</div>
					</a>

					<div class="systems__item_arrows"></div>
					<div class="systems__item_img">
						<img src="../assets/images/system_img1.png" alt="">
					</div>
				</div>
			</div>
			<div class="systems__col">
				<div class="systems__item">
					<h3>Система лояльности <span>для частных лиц</span></h3>
					<ul>
						<li>Помощь в проектировании комплектации</li>
						<li>Срочные поставки до объекта</li>
						<li>Отсрочка платежа</li>
					</ul>

					<a href="clients.php" class="our_products__btn button-arrow icon-right">
						<span>узнать больше</span>
						<div class="icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
								<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
							</svg>
						</div>
					</a>

					<div class="systems__item_arrows"></div>
					<div class="systems__item_img">
						<img src="../assets/images/system_img2.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section solutions solutions--hidden-el">
	<div class="site-container">
		<div class="main-title rest">
			<h2>10 комплексных решений <span>для строительной индустрии</span></h2>
			<p><strong>Доставим оборудование</strong> до объекта</p>
			<p>собственным корпоративным транспортом</p>
		</div>
		<div class="solutions__list main_list">

			<?
				$product_arr_second = ["Запорная арматура", "Трубы", "Насосное оборудование", "Котельное  и теплообменное  оборудование", "Пожарное оборудование", "Теплоизоляционное  оборудование", "Крепежные системы", "Емкостное оборудование", "Приборы учета и&nbsp;КИПиА", "Газовое оборудование"];
				$num_second = 0;
			?>

			<? foreach ($product_arr_second as $val) {?>
				<? $num_second++; ?>
				<div class="main_list__col">
					<a href="page_<? echo $num_second; ?>.php" class="main_list__item" style="background-image: url(../assets/images/solutions_img1<? echo $num_second; ?>.jpg)">
						<div class="main_list__item_text">
							<p class="main_list__text"><? echo $val; ?></p>
							<p class="main_list__text--hidden">
								Односедельные и двухседельные,
								нормально открытые и закрытые
							</p>

						</div>

						<div class="our_products__item_btn item_btn">
							<span class="item_btn--hover" data-text-2="Подоробнее" data-text="ОТ 17 000 РУБЛЕЙ" >Подробнее</span>
							<div class="item_btn__icon">
								<span class="icon-right-arrow arrow-right"></span>
								<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
									<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
								</svg>
							</div>
						</div>
					</a>
				</div>
			<? } ?>

			<div class="main_list__col last-child solutions--show-el">
				<a href="" class="main_list__item">
					<div class="main_list__cube">
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
						<div><span></span></div>
					</div>

					<div class="main_list__show_all">
						Смотреть все решения
					</div>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="section portfolio">
	<div class="portfolio__bottom_line"></div>
	<div class="portfolio__img">
		<img src="../assets/images/porfolio_map.png" alt="">
	</div>


	<div class="site-container">
		<div class="main-title rest">
			<h2><span>Укомплектовали</span> 12 000 объектов в РФ и СНГ</h2>
			<p><strong>Полная комплектация объектов</strong> любого</p>
			<p>масштаба: от квартиры до микрорайона</p>
		</div>

		<div class="custom-arrows portfolio__arrow-container"></div>


		<div class="portfolio__slider">
			<div class="portfolio__item">
				<div class="portfolio__item_title">
					2012
				</div>
				<div class="portfolio__item_text">
					<ul>
						<li> <span>Объект:</span><strong>ООО «Гринхауз»</strong></li>
						<li> <span>Geo:</span><strong>г. Старый Оскол</strong></li>
					</ul>
				</div>

				<div class="portfolio__item_results results">
					<div class="results__top">
						<p>Задача:</p>
						<p>поставка гофрированной трубы для строительства комбикормового завода</p>
					</div>
					<div class="results__bottom">
						<p>Результат:</p>
						<p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
					</div>
					<div class="results__price">
						25 0000 рублей
					</div>
				</div>
			</div>
			<div class="portfolio__item">
				<div class="portfolio__item_title">
					2013 - 2014
				</div>
				<div class="portfolio__item_text">
					<ul>
						<li> <span>Объект:</span><span><strong>ОАО «Мираторг»</strong></span></li>
						<li> <span>Geo:</span><span><strong>Пос. Кировский в Курской области</strong></span></li>
					</ul>
				</div>

				<div class="portfolio__item_results results">
					<div class="results__top">
						<p>Задача:</p>
						<p>поставка гофрированной трубы для строительства комбикормового завода</p>
					</div>
					<div class="results__bottom">
						<p>Результат:</p>
						<p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
					</div>
					<div class="results__price">
						25 0000 рублей
					</div>
				</div>
			</div>
			<div class="portfolio__item">
				<div class="portfolio__item_title">
					2018
				</div>
				<div class="portfolio__item_text">
					<ul>
						<li> <span>Объект:</span><span><strong>ГК «Агробелогорье»</strong></span></li>
						<li> <span>Geo:</span><span><strong>Борисовский район  Белгородской области</strong></span></li>
					</ul>
				</div>

				<div class="portfolio__item_results results">
					<div class="results__top">
						<p>Задача:</p>
						<p>поставка гофрированной трубы для строительства комбикормового завода</p>
					</div>
					<div class="results__bottom">
						<p>Результат:</p>
						<p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
					</div>
					<div class="results__price">
						25 0000 рублей
					</div>
				</div>
			</div>
			<div class="portfolio__item">
				<div class="portfolio__item_title">
					2016
				</div>
				<div class="portfolio__item_text">
					<ul>
						<li> <span>Объект:</span><span><strong>ООО «Гринхауз»</strong></span></li>
						<li> <span>Geo:</span><span><strong>г. Старый Оскол</strong></span></li>
					</ul>
				</div>

				<div class="portfolio__item_results results">
					<div class="results__top">
						<p>Задача:</p>
						<p>поставка гофрированной трубы для строительства комбикормового завода</p>
					</div>
					<div class="results__bottom">
						<p>Результат:</p>
						<p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
					</div>
					<div class="results__price">
						25 0000 рублей
					</div>
				</div>
			</div>
			<div class="portfolio__item">
				<div class="portfolio__item_title">
					2020
				</div>
				<div class="portfolio__item_text">
					<ul>
						<li> <span>Объект:</span><span><strong>ООО «Гринхауз»</strong></span></li>
						<li> <span>Geo:</span><span><strong>г. Старый Оскол</strong></span></li>
					</ul>
				</div>

				<div class="portfolio__item_results results">
					<div class="results__top">
						<p>Задача:</p>
						<p>поставка гофрированной трубы для строительства комбикормового завода</p>
					</div>
					<div class="results__bottom">
						<p>Результат:</p>
						<p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
					</div>
					<div class="results__price">
						25 0000 рублей
					</div>
				</div>
			</div>
			<div class="portfolio__item">
				<div class="portfolio__item_title">
					2012
				</div>
				<div class="portfolio__item_text">
					<ul>
						<li> <span>Объект:</span><span><strong>ООО «Гринхауз»</strong></span></li>
						<li> <span>Geo:</span><span><strong>г. Старый Оскол</strong></span></li>
					</ul>
				</div>

				<div class="portfolio__item_results results">
					<div class="results__top">
						<p>Задача:</p>
						<p>поставка гофрированной трубы для строительства комбикормового завода</p>
					</div>
					<div class="results__bottom">
						<p>Результат:</p>
						<p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
					</div>
					<div class="results__price">
						25 0000 рублей
					</div>
				</div>
			</div>
		</div>

		<div class="text-center">
			<a class="portfolio__see_all" href="projects.php">Посмотреть все объекты</a>
		</div>

	</div>
</section>

<section class="section cooperation">

	<div class="cooperation__anim-arrow"></div>

	<div class="block-wrapper">
		<div class="systems__bloks">
			<div></div>
			<div></div>
		</div>
	</div>

	<div class="site-container">
		<div class="main-title">
			<h2>63% клиентов сотрудничают <span>с нами от 3 лет и более</span></h2>
		</div>

		<div class="custom-arrows cooperation__arrow-container"></div>


		<div class="cooperation__slider">
			<div class="cooperation__item">
				<a data-fancybox href="../assets/images/cooperation_img3.jpg">
					<img src="../assets/images/cooperation_img3.jpg" alt="">
				</a>
			</div>
			<div class="cooperation__item">
				<a data-fancybox href="../assets/images/cooperation_img3.jpg">
					<img src="../assets/images/cooperation_img3.jpg" alt="">
				</a>
			</div>
			<div class="cooperation__item">
				<a data-fancybox href="../assets/images/cooperation_img3.jpg">
					<img src="../assets/images/cooperation_img3.jpg" alt="">
				</a>
			</div>
			<div class="cooperation__item">
				<a data-fancybox href="../assets/images/cooperation_img3.jpg">
					<img src="../assets/images/cooperation_img3.jpg" alt="">
				</a>
			</div>
			<div class="cooperation__item">
				<a data-fancybox href="../assets/images/cooperation_img3.jpg">
					<img src="../assets/images/cooperation_img3.jpg" alt="">
				</a>
			</div>
		</div>

	</div>
</section>

<section class="section footer_section">
	<div class="site-container">
		<div class="footer_section__img">
			<img src="../assets/images/form_img.png" alt="">
		</div>

		<div class="footer_section__row">
			<div class="footer_section__col">
				<div class="footer_section__item">
					<div class="main-title">
						<h2>Получите расчет стоимости <span>проекта</span></h2>
					</div>

					<ul>
						<li><strong>Подберем выгодное коммерческое предложение</strong> под вашу задачу.</li>
						<li><strong>Предложим 3 варианта комплектации</strong> объекта в рамках бюджета.</li>
						<li><strong>Рассчитаем проект с учетом отсрочки</strong> платежа – по договоренности</li>
					</ul>
				</div>
			</div>
			<div class="footer_section__col">
				<div class="footer_section__item">
					<div class="form_custom">
						<form action="" class="formValidate validate" name="main-form">
							<h5>Оставьте заявку,</h5>
							<p>и мы перезвоним вам и сделаем расчет</p>
							<p>стоимости комплектации вашего объекта</p>

							<div class="input_wrapper">
								<div class="input_container">
									<input type="text" placeholder="Ваше имя" name="name" class="required" data-mask="fio">
								</div>
								<div class="input_container">
									<input type="text" placeholder="Ваш номер" name="tell" class="required" data-mask="phone">
								</div>
								<div class="input_container">
									<input type="text" placeholder="Ваша почта" name="e-mail" class="required" data-mask="email">
								</div>
							</div>

							<div class="text-center">
								<button class="button" type="submit">Оставить заявку</button>
							</div>

							<label class="form-agreement">
								<input class="form-agreement__input required" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
								<span class="form-agreement__text">
									<span class="form-agreement__check"></span>
									Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
								</span>
							</label>

						</form>
					</div>
				</div>
			</div>
		</div>





	</div>
</section>

<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php' ?>
