<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
	<div class="site-container">
		<? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
	</div>

	<section class="section page-catalog">
		<div class="page-catalog__image">
			<img src="../assets/images/pages/catalog/hero-img.png" alt="Погрузщик">
		</div>
		<div class="site-container">
			<div class="page-catalog__title main-title">
				<h1>
					20&nbsp;000 единиц оборудования <br> <span>для инженерных сетей в&nbsp;наличии на&nbsp;складе</span>
				</h1>
			</div>

			<p class="page-catalog__text">
				Продукция от&nbsp;35&nbsp;всемирно известных брендов с&nbsp;экспресс-доставкой по&nbsp;России
			</p>

			<ul class="hero_section__list">
				<li class="hero_section__item">
					Круглосуточный склад адресного хранения площадью 5&nbsp;600&nbsp;м&sup2; и&nbsp;мощностью 9&nbsp;600 паллетомест
				</li>
				<li class="hero_section__item">
					Благодаря 300 дилерским соглашениям продукция дешевле рынка&nbsp;&mdash; от&nbsp;5% до&nbsp;30%
				</li>
				<li class="hero_section__item">
					Сертификация СМК по&nbsp;стандарту ISO 9001
				</li>
			</ul>

			<div class="btn_wrap">
				<button class="button" data-popup="spez">Получить спецпредложение</button>
				<p>Оставьте заявку и получите выгодные <br> условия сотрудничества</p>
			</div>
		</div>
	</section>

	<section class="section catalog-products">
		<div class="site-container">
			<div class="catalog-products__row">
				<div class="catalog-products__col">
					<span class="catalog-products__legend" data-parent="cat">
						Каталог
					</span>
					<div class="catalog-products__wrapper" data-child="cat">
						<ul class="catalog-products__list">
							<li class="catalog-products__item active" data-cat="cat-name">
								<span class="catalog-products__item-legend active js--show-submenu" >
									Запорная арматура
									<span class="icon-right-arrow"></span>
								</span>

								<ul class="catalog-products__sublist" style="display: none">
									<li class="catalog-products__subitem" data-cat="cat-name">
										<a href="javascript:void(0);">Задвижки</a>
									</li>
									<li class="catalog-products__subitem" data-cat="cat-name">
										<a href="javascript:void(0);">Вентили (клапаны)</a>
									</li>
									<li class="catalog-products__subitem" data-cat="cat-name">
										<a href="javascript:void(0);">Краны шаровые</a>
									</li>
									<li class="catalog-products__subitem" data-cat="cat-name">
										<a href="javascript:void(0);">Фитинги</a>
									</li>
								</ul>
							</li>
							<li class="catalog-products__item" data-cat="cat-name">
								<a href="javascript:void(0);">Приборы учета и КИПиА</a>
							</li>
							<li class="catalog-products__item" data-cat="cat-name">
								<a href="javascript:void(0);">Газовое оборудование</a>
							</li>
							<li class="catalog-products__item" data-cat="cat-name">
								<a href="javascript:void(0);">Крепежные системы</a>
							</li>
							<li class="catalog-products__item" data-cat="cat-name">
								<a href="javascript:void(0);">Насосное оборудование</a>
							</li>
							<li class="catalog-products__item" data-cat="cat-name">
								<a href="javascript:void(0);">Котельное и теплообменное оборудование</a>
							</li>
							<li class="catalog-products__item" data-cat="cat-name">
								<a href="javascript:void(0);">Теплоизоляционное оборудование</a>
							</li>
							<li class="catalog-products__item" data-cat="cat-name">
								<a href="javascript:void(0);">Пожарное оборудование</a>
							</li>
							<li class="catalog-products__item" data-cat="cat-name">
								<a href="javascript:void(0);">Трубы и комплектующие</a>
							</li>
						</ul>
					</div>

					<span class="catalog-products__legend" data-parent="brand">
						Выберите бренд
					</span>
					<div class="catalog-products__wrapper" data-child="brand">
						<form class="catalog-products__search formValidate validate" action="/app/test.php">
							<input class="catalog-products__input required" type="text" placeholder="Поиск значений" name="brand-name" data-mask="letters">
							<button type="submit" class="button-reset catalog-products__search-button" ><span class="icon-search js--submit-brands"></span></button>
						</form>

						<ul class="catalog-products__list --brands-list">
							<li class="catalog-products__item catalog-products__item--brand-item" data-cat="brand-name">
								<a href="javascript:void(0);">Danfoss</a>
							</li>
							<li class="catalog-products__item catalog-products__item--brand-item" data-cat="brand-name">
								<a href="javascript:void(0);">K-FLEX</a>
							</li>
							<li class="catalog-products__item catalog-products__item--brand-item" data-cat="brand-name">
								<a href="javascript:void(0);">Hawle</a>
							</li>
							<li class="catalog-products__item catalog-products__item--brand-item" data-cat="brand-name">
								<a href="javascript:void(0);">Wilo</a>
							</li>
							<li class="catalog-products__item catalog-products__item--brand-item" data-cat="brand-name">
								<a href="javascript:void(0);">Grundfos</a>
							</li>
							<li class="catalog-products__item catalog-products__item--brand-item" data-cat="brand-name">
								<a href="javascript:void(0);">IMI</a>
							</li>
							<li class="catalog-products__item catalog-products__item--brand-item" data-cat="brand-name">
								<a href="javascript:void(0);">АДЛ</a>
							</li>
						</ul>

						<div class="catalog-products__brands--show-more">
							<span>
								Показать еще...
							</span>
						</div>
					</div>

					<span class="catalog-products__legend" data-parent="price">
						Цена
					</span>
					<div class="catalog-products__wrapper" data-child="price">
						<div class="catalog-products__price">
							<div class="catalog-products__input-wrap">
								<input type="number" class="catalog-products__price-input" id="input0">
								<input type="number" class="catalog-products__price-input" id="input1">
							</div>
							<div class="catalog-products__price-line" id="range-slider"></div>
						</div>
					</div>
				</div>

				<div class="catalog-products__col">
					<div class="catalog-products__sort">
						<span class="catalog-products__sort-label">
							<span class="catalog-products__sort-name">Сначала дорогие</span>
							<span class="icon-right-arrow catalog-products__sort-btn"></span>
						</span>

						<ul class="catalog-products__sort-list" style="display:none">
							<li class="catalog-products__sort-item" data-sort="sort1">
								Сначала дорогие1
							</li>
							<li class="catalog-products__sort-item" data-sort="sort2">
								Сначала дорогие2
							</li>
							<li class="catalog-products__sort-item" data-sort="sort3">
								Сначала дорогие3
							</li>
						</ul>

					</div>

					<?
						$arr = ["Запорная арматура", "Газорегуляторное  оборудование", "Насосное оборудование", "Приборы учета газа", "Теплоизоляционное  оборудование", "Крепежные системы", "Счетчики воды", "Задвижка стальная 30с541нж Ру16 МЗТА", "Насосы общего  применения", "Отводы ГОСТ 17375-2001", "Труба армированная,  зачистная – PN 25", "Краны шаровые"];
						$num = 0;
					?>

					<div class="main_list catalog-products__container" id="catalog-container">

						<? foreach ($arr as $val) {?>
							<? $num++; ?>
							<div class="main_list__col catalog-products__container-col">
								<a href="cart.php" class="main_list__item catalog-products__container-item" style="background-image: url(../assets/images/pages/catalog/product_img<? echo $num; ?>.jpg)">
									<div class="main_list__item_text">
										<p class="main_list__text"><? echo $val; ?></p>
									</div>

									<div class="our_products__item_btn item_btn">
										<span class="item_btn--hover" data-text-2="Подоробнее" data-text="ОТ 17 000 РУБЛЕЙ" >Подробнее</span>
										<div class="item_btn__icon">
											<span class="icon-right-arrow arrow-right"></span>
											<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
												<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
											</svg>
										</div>
									</div>
								</a>
							</div>
						<? } ?>
					</div>

					<div class="catalog-products__pagination">
						<? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/pagination.php' ?>
					</div>
				</div>
			</div>
		</div>
	</section>



<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
