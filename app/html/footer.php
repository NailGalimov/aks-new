</main>

<footer class="footer_custom">
	<div class="footer_custom__arrows"></div>

	<div class="site-container">
		<div class="footer_custom__row">
			<div class="footer_custom__col">
				<div class="logo__wrapper">
					<a class="footer_custom__logo" href="#">
						<img src="../assets/images/footer_logo.png" alt="">
					</a>
					<p>Снабжение строительных объектов инженерными системами и оборудованием</p>
				</div>
				<div class="footer_custom__copyright">
					ⓒ Все права защищены. Информация
					на сайте не является публичной офертой.
					ИНН: 00000000000
				</div>

			</div>
			<div class="footer_custom__col">
				<ul>
					<li><a href=""> О компании </a></li>
					<li><a href=""> Доставка </a></li>
					<li><a href=""> Каталог </a></li>
					<li><a href=""> Клиентам </a></li>
					<li><a href=""> Услуги </a></li>
					<li><a href=""> Контакты </a></li>
					<li><a href=""> Проекты </a></li>
				</ul>
			</div>
			<div class="footer_custom__col">
				<div class="footer_custom__contacts footer_contacts">
					<div class="footer_contacts__item_wrap">
						<div class="footer_contacts__name">
							г. Белгород
						</div>
						<div class="footer_contacts__links">
							<a href="tel:+74722568082">+7 (4722) 56-80-82</a>
							<a href="#">acs.31@yandex.ru</a>
						</div>
					</div>
					<div class="footer_contacts__item_wrap">
						<div class="footer_contacts__name">
							г. Курск
						</div>
						<div class="footer_contacts__links">
							<a href="tel:+74712343009">+7 (4712) 34-30-09</a>
							<a href="#">acs46@yandex.ru</a>
						</div>
					</div>
				</div>
			</div>
			<div class="footer_custom__col">
				<div class="b2b-copy">
					<a href="" target="_blank" class="b2b-copy-link">Разработано</a>	<a href="" target="_blank" class="b2b-copy-svg">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 316.1 99" role="img">
							<title>маркетинговое агентство </title>
							<circle cx="49.5" cy="49.5" r="49.5" fill="#fa0"></circle>
							<path fill="#1D1A24" d="M53.2 46.9l-2.8-9.6L9.5 78.7l16.4-11.9 20-14.5 2.3 7.8 41.4-39.6-36.4 26.4zm29.2-3.5L66.7 58.7l15.5-10.8 7.5 5.8-7.3-10.3zm-55.9 0l-19.3 20L26.4 49l9.8 7.1-9.7-12.7z"></path>
							<path id="b2b" d="M239 79.6h10.3v-9.8L239 79.6zm7-11.1l-25.9.1c.1-3.8 6-6.9 12.9-11.1 7.6-4.5 15.8-10.4 15.8-20.2 0-11.9-9.5-17.2-21.3-17.2-7.2-.1-14.2 2-20.1 6.2v12c6.2-4.7 13-6.9 18.9-6.9s9.7 2.2 9.7 6.5c0 5.6-5.2 7.9-11.3 11.4-8.6 5.1-19.2 10.5-19.2 24.6v5.7h25.3L246 68.5zM140.3 22.2l4.1 14.2 23.1-16.8c-.3 0-.7 0-1 0h-26.3l.1 2.6zm37.9 25.6c5.3-1.8 9.1-6.1 9.1-13.1 0-8-4.9-12.8-13.2-14.5L163.8 30h1.6c5.7 0 9.1 2.1 9.1 6.9s-3.5 6.9-8.4 6.9h-12.9v-3.5l-12.9 12.3v27h25.8c10.5 0 23.6-3.2 23.6-16.8 0-8.9-5.6-13.3-11.5-15zm-12.3 21.3H153V53.7h13.5c5.1 0 10.2 1.4 10.2 7.7.1 6.5-5 7.7-10.8 7.7zm100.8-46.9l4.1 14.2L294 19.6c-.3 0-.7 0-1 0h-26.3v2.6zm37.9 25.6c5.3-1.8 9.1-6.1 9.1-13.1 0-8-4.9-12.8-13.2-14.5L290.2 30h1.6c5.7 0 9.1 2.1 9.1 6.9s-3.5 6.9-8.4 6.9h-12.9v-3.5l-12.9 12.3v27h25.8c10.5 0 23.6-3.2 23.6-16.8 0-8.9-5.6-13.3-11.5-15zm-12.3 21.3h-12.9V53.7H293c5.1 0 10.2 1.4 10.2 7.7 0 6.5-5.1 7.7-10.9 7.7z"></path>
						</svg>
					</a>
				</div>
				<div class="footer_custom__copyright">
					ⓒ Все права защищены. Информация
					на сайте не является публичной офертой.
					ИНН: 00000000000
				</div>
			</div>
		</div>
	</div>
</footer>
<script src="../assets/js/libs.min.js"></script>
<script src="../assets/js/popup.js"></script>
<script src="../assets/js/ajax.js"></script>
<script src="../assets/js/main.js"></script>
</body>
</html>
