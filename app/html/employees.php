<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
<div class="site-container">
    <? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
</div>

<section class="section employees-hero">
    <div class="employees-hero__image">
        <img src="../assets/images/pages/employees/hero-img.png" alt="Бейдж">
    </div>
    <div class="site-container">
        <div class="employees-hero__title main-title">
            <h1>
                80+&nbsp;экспертов в&nbsp;области оборудования <span>для&nbsp;инженерных сетей</span>
            </h1>

            <p>
                Комплектуем инвестпроекты, внедряем инновации, ищем лучшие решения
            </p>
        </div>

        <div class="btn_wrap">
            <button class="button" data-popup="consultation">Получить консультацию</button>
            <p>Получите консультацию технического <br> специалиста бесплатно</p>
        </div>
    </div>
</section>

<section class="section reasons --page2 employees-reasons">
    <div class="site-container">

        <div class="main-title">
            <h2>
                Мощные достижения команды сотрудников
            </h2>
        </div>

        <div class="feautures__inner" style="background-image: url(../assets/images/feautures_bg.jpg)">

            <div class="feautures__list">
                <div class="feautures__item">
                    <h3><span class="countAnimation" data-text="100">100</span><span class="color-red">+</span><span
                            class="text_m">соглашений</span></h3>
                    <p>С заводами - производителями</p>
                </div>
                <div class="feautures__item">
                    <h3><span class="countAnimation" data-text="12000">12 000</span><span class="text_m">объектов</span>
                    </h3>
                    <p>укомплектовали под ключ</p>
                </div>
                <div class="feautures__item">
                    <h3><span class="countAnimation" data-text="10">10</span> <span class="text_m">готовых
                            решений</span></h3>
                    <p>для строительной индустрии</p>
                </div>
                <div class="feautures__item">
                    <h3><span class="countAnimation" data-text="1000">1000</span><span class="color-red">+</span> <span
                            class="text_m">клиентов</span></h3>
                    <p>во всех регионах России</p>
                </div>
                <div class="feautures__item">
                    <h3><span class="countAnimation" data-text="24">24</span><span class="text_m">в сутки</span></h3>
                    <p>работает склад на 9 600 паллетомест</p>
                </div>
                <div class="feautures__item">
                    <h3><span class="color-red">От</span> <span class="countAnimation" data-text="1">1</span> <span
                            class="text_m">дня</span></h3>
                    <p>доставка собственным транспортом</p>
                </div>
            </div>

        </div>

    </div>
</section>

<section class="section employees-chief">
    <div class="employees-chief__image">
        <img src="../assets/images/pages/about/map-2.png" alt="Снабжаем строительные объекты оборудованием под ключ">
    </div>
    <div class="employees-chief__image man">
        <img src="../assets/images/pages/about/man.png" alt="Снабжаем строительные объекты оборудованием под ключ">
    </div>

    <div class="site-container">
        <div class="employees-chief__title main-title">
            <h2>
                &laquo;Каждый день решаем сложные инженерные задачи&raquo;
            </h2>
        </div>

        <div class="employees-chief__text">
            <img class="sign" src="../assets/images/pages/about/sign.svg" alt="">

            <p>
                Наша компания начинала работу как обычный поставщик инженерного и&nbsp;сантехнического оборудования.
                Амбиции, возможности и&nbsp;квалификация кадров позволили нам шагнуть дальше и&nbsp;комплектовать под
                ключ инвестпроекты по&nbsp;всей России.
            </p>
            <p>
                Мы&nbsp;не&nbsp;просто продаем инженерные системы. Каждый день более 80&nbsp;сотрудников занимаются
                разработкой проектов, придумывают, как внедрять новинки, предлагают варианты комплектации под
                нестандартные задачи
            </p>
            <p>
                На&nbsp;выходе мы&nbsp;предоставляем клиентам готовые, продуманные инженерные решения от&nbsp;одного
                поставщика.
            </p>

            <p class="employees-chief__name">
                Дерюгин Андрей Федорович
            </p>

            <p class="employees-chief__position">
                Генеральный директор компании <span>“АрмКомплексСнаб”</span>
            </p>
        </div>
    </div>
</section>

<section class="section employees-people">
	<div class="_circle-big"></div>
	<div class="_circle-line">
		<img src="../assets/images/pages/employees/line-circle.svg" alt="#">
	</div>
	<div class="_circle-line-small">
		<img src="../assets/images/pages/employees/line-circle-small.svg" alt="#">
	</div>
	<div class="site-container">
		<div class="employees-people__title">
			<h2>
				Сотрудники с&nbsp;опытом от&nbsp;3&nbsp;лет
			</h2>
		</div>

		<div class="employees-people__container">
			<div class="_circle-small"></div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Савельева Кристина Владимировна
				</span>

				<span class="employees-people__position">
					Помщник директора
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/1.jpg" alt="Помощник директора">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/1-2.jpg" alt="Помощник директора">
				</div>
			</div>
			<div class="employees-people__item">
				<img class="employees-people__image" src="../assets/images/pages/employees/people/2.jpg" alt="Помощник директора">
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Капустин Андрей Алексеевич
				</span>

				<span class="employees-people__position">
					Менеджер отдела продаж
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/3.jpg" alt="Менеджер отдела продаж">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/3-1.jpg" alt="Менеджер отдела продаж">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Поспелова Марина Игоревна
				</span>

				<span class="employees-people__position">
					Главный бухгалтер
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/4.jpg" alt="Главный бухгалтер">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/4-1.jpg" alt="Главный бухгалтер">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Кустова Анна Игоревна
				</span>

				<span class="employees-people__position">
					Бухгалтер по реализации
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/5.jpg" alt="Бухгалтер по реализации">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/5-1.jpg" alt="Бухгалтер по реализации">
				</div>
			</div>
			<div class="employees-people__item">
				<img class="employees-people__image" src="../assets/images/pages/employees/people/6.jpg" alt="Помощник директора">
			</div>
			<div class="employees-people__item">
				<img class="employees-people__image" src="../assets/images/pages/employees/people/7.jpg" alt="Помощник директора">
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Майер Виктория Александровна
				</span>

				<span class="employees-people__position">
					Менеджер отдела продаж
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/8.jpg" alt="Менеджер отдела продаж">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/8-1.jpg" alt="Менеджер отдела продаж">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Абраменко Ирина Александровна
				</span>

				<span class="employees-people__position">
					Бухгалтер
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/9.jpg" alt="Бухгалтер">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/9-1.jpg" alt="Бухгалтер">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Дерюгин Андрей Федорович
				</span>

				<span class="employees-people__position">
					Директор
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/11.jpg" alt="Директор">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/11-1.jpg" alt="Директор">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Дерюгин Юрий Федорович
				</span>

				<span class="employees-people__position">
					Коммерческий директор
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/12.jpg" alt="Коммерческий директор">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/12-1.jpg" alt="Коммерческий директор">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Кириленко Елена Сергеевна
				</span>

				<span class="employees-people__position">
					Ведущий менеджер по закупкам
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/13.jpg" alt="Ведущий менеджер по закупкам">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/13-1.jpg" alt="Ведущий менеджер по закупкам">
				</div>
			</div>
			<div class="employees-people__item">
				<img class="employees-people__image" src="../assets/images/pages/employees/people/14.jpg" alt="Менеджер отдела продаж">
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Савоненко Татьяна Ивановна
				</span>

				<span class="employees-people__position">
					Специалист по кадровому делу
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/15.jpg" alt="Специалист по кадровому делу">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/15-1.jpg" alt="Специалист по кадровому делу">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Новиков Александр Михайлович
				</span>

				<span class="employees-people__position">
					Руководитель обособленного подразделения в г. Липецк
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/16.jpg" alt="Руководитель обособленного подразделения в г. Липецк">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/16-1.jpg" alt="Руководитель обособленного подразделения в г. Липецк">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Козлов Сергей Анатольевич
				</span>

				<span class="employees-people__position">
					Руководитель отдела продаж
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/17.jpg" alt="Руководитель отдела продаж">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/17-1.jpg" alt="Руководитель отдела продаж">
				</div>
			</div>
			<div class="employees-people__item">
				<img class="employees-people__image" src="../assets/images/pages/employees/people/18.jpg" alt="Менеджер отдела продаж">
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Алб Светлана Викторовна
				</span>

				<span class="employees-people__position">
					Менеджер отдела продаж
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/19.jpg" alt="Менеджер отдела продаж">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/19-1.jpg" alt="Менеджер отдела продаж">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Баркова Светлана Александровна
				</span>

				<span class="employees-people__position">
					Менеджер по закупкам
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/20.jpg" alt="Менеджер по закупкам">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/21-1.jpg" alt="Менеджер по закупкам">
				</div>
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Клавкина Татьяна Владимировна
				</span>

				<span class="employees-people__position">
					Бухгалтер
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/21.jpg" alt="Бухгалтер">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/21-1.jpg" alt="Бухгалтер">
				</div>
			</div>
			<div class="employees-people__item">
				<img class="employees-people__image" src="../assets/images/pages/employees/people/22.jpg" alt="Менеджер отдела продаж">
			</div>
			<div class="employees-people__item">
				<img class="employees-people__image" src="../assets/images/pages/employees/people/23.jpg" alt="Менеджер отдела продаж">
			</div>
			<div class="employees-people__item">
				<span class="employees-people__legend">
					Сологуб Владимир Сергеевич
				</span>

				<span class="employees-people__position">
					Главный инженер
				</span>

				<div class="employees-people__wrap">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/24.jpg" alt="Главный инженер">
					<img class="employees-people__image" src="../assets/images/pages/employees/people/24-1.jpg" alt="Главный инженер">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section footer_section footer_section--page projects-form employees-form">

    <div class="employees-form__img --hr">
        <img src="../assets/images/pages/employees/hr.png" alt="HR менеджер">
    </div>

    <div class="site-container">
        <div class="footer_section__row">
            <div class="footer_section__col">
                <div class="footer_section__item">
                    <div class="main-title" style="opacity: 1; transform: translate(0px, 0px);">
                        <h2>Станьте частью команды <span>надежной федеральной компании</span></h2>
                    </div>

                    <ul>
                        <li>
							<strong>Предлагаем интересные задачи,</strong> которые станут профессиональным вызовом;
                        </li>
                        <li>
							<strong>Даем возможность для карьерного роста</strong> квалифицированным специалистам;
                        </li>
						<li>
							<strong>Соблюдаем все договоренности</strong> с нашими сотрудниками.
						</li>
                    </ul>
                </div>
            </div>
            <div class="footer_section__col">
                <div class="footer_section__item">
                    <div class="form_custom">
                        <form action="" class="formValidate validate" name="main-form" novalidate="novalidate">
                            <h5>Отправьте заявку</h5>
                            <p>&mdash;&nbsp;HR-менеджер с&nbsp;вами свяжется.</p>

                            <div class="input_wrapper">
                                <div class="input_container">
                                    <input type="text" placeholder="Ваше имя" name="name" class="required"
                                        data-mask="fio">
                                </div>
                                <div class="input_container">
                                    <input type="text" placeholder="Ваш номер" name="tell" class="required"
                                        data-mask="phone">
                                </div>
                                <div class="input_container">
                                    <input type="text" placeholder="Ваша почта" name="e-mail" class="required"
                                        data-mask="email">
                                </div>
                            </div>

                            <div class="text-center">
                                <button class="button" type="submit">Оставить заявку</button>
                            </div>

                            <label class="form-agreement">
                                <input class="form-agreement__input required" type="checkbox" checked="checked"
                                    value="Согласие на обработку данных" name="Agreement">
                                <span class="form-agreement__text">
                                    <span class="form-agreement__check"></span>
                                    Я даю свое согласие на обработку персональных данных и соглашаюсь с <a
                                        href="">политикой конфиденциальности</a>
                                </span>
                            </label>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
