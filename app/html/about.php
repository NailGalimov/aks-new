<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/header.php'?>
<div class="site-container">
    <? include $_SERVER['DOCUMENT_ROOT'].'/app/include/pages/breadcrumbs.php' ?>
</div>

<section class="section about-hero">
    <div class="about-hero__image">
        <img src="../assets/images/pages/about/hero-img.png"
            alt="Комплектуем инвестпроекты инженерными системами под ключ">
    </div>
    <div class="about-hero__image --car">
        <img src="../assets/images/pages/about/hero-img-car.png"
            alt="Комплектуем инвестпроекты инженерными системами под ключ">
    </div>
    <div class="site-container">
        <div class="about-hero__title main-title">
            <h1>
                Компания-интегратор &laquo;АрмКомплексСнаб&raquo; <span>Комплектуем инвестпроекты инженерными системами
                    под ключ</span>
            </h1>
        </div>

        <ul class="hero_section__list">
            <li class="hero_section__item">
                20&nbsp;000 единиц сертифицированной продукции в&nbsp;наличии на&nbsp;складе
            </li>
            <li class="hero_section__item">
                Поставили 65&nbsp;000&nbsp;км полиэтиленовых труб&nbsp;&mdash; больше длины экватора
            </li>
            <li class="hero_section__item">
                Наши насосы перекачали объем воды, равный реке Дон
            </li>
        </ul>

        <div class="btn_wrap">
            <button class="button" data-popup="consultation">Получить консультацию</button>
            <p>Получите консультацию технического <br> специалиста бесплатно</p>
        </div>
    </div>
</section>
<section class="section about-second">
    <div class="about-second__image">
        <img src="../assets/images/pages/about/about-second.png"
            alt="Снабжаем строительные объекты оборудованием под ключ">
    </div>
    <div class="site-container">
        <div class="about-second__title main-title">
            <h2>
                Снабжаем строительные объекты <span>оборудованием под ключ</span>
            </h2>
        </div>

        <div class="about-second__text">
            <p>
                Снабжаем сантехническим и&nbsp;инженерным оборудованием строительные и&nbsp;монтажные компании,
                юридических и&nbsp;физических лиц. Внедряем инновационные новинки, перерабатываем проекты, предлагая
                лучшие аналоги.
            </p>
            <p>
                Осуществляем комплексные поставки продукции от&nbsp;всемирно известных брендов по&nbsp;всей России
                и&nbsp;СНГ. Благодаря сотне дилерских соглашений и&nbsp;наработанному опыту наши товары дешевле
                рынка&nbsp;&mdash; до&nbsp;30%.
            </p>
            <p>
                Главное преимущество нашей компании&nbsp;&mdash; полная комплектация объектов любого масштаба:
                от&nbsp;квартиры и&nbsp;завода до&nbsp;целого микрорайона и&nbsp;индустриального комплекса.
            </p>
            <p>
                С&nbsp;2009 года обеспечили реализацию оборудования для водоснабжения, отопления и&nbsp;газоснабжения
                на&nbsp;12&nbsp;000&nbsp;объектов.
            </p>
        </div>
    </div>
</section>
<section class="section employees-chief">
    <div class="employees-chief__image">
        <img src="../assets/images/pages/about/map-2.png" alt="Снабжаем строительные объекты оборудованием под ключ">
    </div>
    <div class="employees-chief__image man">
        <img src="../assets/images/pages/about/man.png" alt="Снабжаем строительные объекты оборудованием под ключ">
    </div>

    <div class="site-container">
        <div class="employees-chief__title main-title">
            <h2>
                &laquo;Каждый день решаем сложные инженерные задачи&raquo;
            </h2>
        </div>

        <div class="employees-chief__text">
            <img class="sign" src="../assets/images/pages/about/sign.svg" alt="">

            <p>
				&quot;Трубы и&nbsp;трубопроводная арматура&nbsp;&mdash; на&nbsp;самом деле очень важная составляющая нашей жизни. Это водоснабжение, отопление, газоснабжение и&nbsp;технологические процессы практически на&nbsp;всех предприятиях.
            </p>
            <p>
				Мы&nbsp;обеспечиваем жизнедеятельность жилых и&nbsp;коммерческих зданий и&nbsp;сооружений, а&nbsp;значит, и&nbsp;населения страны в&nbsp;целом. Мы&nbsp;работаем, чтобы у&nbsp;каждого человека были комфортные условия жизни: вода, свет и&nbsp;тепло дома и&nbsp;на&nbsp;рабочем месте.&quot;
            </p>

            <p class="employees-chief__name">
                Дерюгин Андрей Федорович
            </p>

            <p class="employees-chief__position">
                Генеральный директор компании <span>“АрмКомплексСнаб”</span>
            </p>
        </div>
    </div>
</section>
<section class="section about-client">
    <div class="site-container">
        <div class="about-client__title main-title">
            <h2>
                1&nbsp;000+ клиентов <span>во&nbsp;всех регионах России</span>
            </h2>
            <p>
                В&nbsp;портфель компаний&nbsp;&mdash; наших партнеров входят &laquo;Газпром&raquo;,
                &laquo;Мосводоканал&raquo; и&nbsp;другие известные организации
            </p>
        </div>
        <div class="about-client__row">
            <? for($i=0; $i<10; $i++) {?>
            <div class="about-client__item">
                <img src="../assets/images/pages/about/img-client<?echo $i;?>.png" alt="Партнеры">
            </div>
            <? } ?>
        </div>
    </div>
</section>
<section class="section reasons --page2 about-reasons">
    <div class="site-container">

        <div class="main-title">
            <h2>
                Почему нас выбирают директоры,
                <span>руководители проектов и снабженцы</span>
            </h2>
        </div>

        <div class="feautures__inner" style="background-image: url(../assets/images/feautures_bg.jpg)">

            <div class="feautures__list">
                <div class="feautures__item">
                    <h3><span class="countAnimation" data-text="300">300</span> <span class="text_m">поставщиков</span>
                    </h3>
                    <p>благодаря дилерским соглашениям продукция дешевле рынка – от 5% до 30%</p>
                </div>
                <div class="feautures__item">
                    <h3><span class="countAnimation" data-text="12000">12 000</span><span class="text_m">объектов</span>
                    </h3>
                    <p>укомплектовали под ключ: от квартиры до микрорайона и строительной площадки</p>
                </div>
				<div class="feautures__item">
                    <h3><span class="countAnimation" data-text="5600">5600</span><span class="color-red">м<sup>2</sup> </span>
                    </h3>
                    <p>площадь собственного склада мощностью 9 600 палетомест  для хранения продукции</p>
                </div>
				<div class="feautures__item">
                    <h3><span class="countAnimation" data-text="20000">20000</span>
					<span class="text_m">единиц</span></h3>
                    <p>сертифицированного оборудования в каталоге компании</p>
                </div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="11">11</span><span class="color-red"> лет</span> <span
							class="text_m">на рынке</span></h3>
					<p>благодаря огромному опыту работаем быстрее конкурентов и гарантируем результат</p>
				</div>
				<div class="feautures__item">
					<h3><span class="countAnimation" data-text="35">35</span> <span class="text_m">производителей</span>
					</h3>
					<p>поставляем продукцию напрямую с отечественных и зарубежных заводов</p>
				</div>
                <div class="feautures__item">
                    <h3><span class="countAnimation" data-text="80">80</span> <span class="color-red">+</span><span
                            class="text_m">сотрудников</span></h3>
                    <p>площадь собственного склада мощностью 9 600 палетомест для хранения продукции</p>
                </div>
                <div class="feautures__item">
                    <h3><span class="countAnimation" data-text="10">10</span><span class="color-red">+</span><span
                            class="text_m">единиц</span></h3>
                    <p>Грузового транспорта в собственном автопарке. Доставка до объекта по РФ</p>
                </div>
            </div>

        </div>

    </div>
</section>
<section class="section clients about-clients">

    <div class="main-title">
        <h2>
            <span>Поставляем продукцию</span> <br> 35 всемирно известных брендов
        </h2>
    </div>

    <div class="clients__wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT']."../app/include/pages/clients.php";?>

        <a href="" class="button-arrow clients__btn">
            <span class="show-text">Посмотреть все</span>
            <div class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
                    <path id="icon-arrow"
                        d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z"
                        transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2">
                    </path>
                </svg>
            </div>
        </a>
    </div>

</section>
<section class="section footer_section footer_section--page projects-form">

    <div class="projects-form__img --calc">
        <img src="../assets/images/pages/projects/calc.png" alt="Калькулятор">
    </div>

    <div class="projects-form__img --tube">
        <img src="../assets/images/pages/projects/tube.png" alt="Трубы">
    </div>

    <div class="site-container">
        <div class="footer_section__row">
            <div class="footer_section__col">
                <div class="footer_section__item">
                    <div class="main-title">
                        <h2>Получите расчет стоимости <span>проекта</span></h2>
                    </div>

                    <ul>
                        <li><strong>Подберем и&nbsp;рассчитаем </strong> необходимое количество продукции под вашу
                            задачу.
                        </li>
                        <li><strong>Сделаем перерасчет материалов</strong> с&nbsp;использованием бюджетных аналогов.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer_section__col">
                <div class="footer_section__item">
                    <div class="form_custom">
                        <form action="" class="formValidate validate" name="main-form" novalidate="novalidate">
                            <h5>Оставьте заявку,</h5>
                            <p>и&nbsp;мы&nbsp;перезвоним вам и&nbsp;сделаем расчет стоимости комплектации вашего объекта
                            </p>

                            <div class="input_wrapper">
                                <div class="input_container">
                                    <input type="text" placeholder="Ваше имя" name="name" class="required"
                                        data-mask="fio">
                                </div>
                                <div class="input_container">
                                    <input type="text" placeholder="Ваш номер" name="tell" class="required"
                                        data-mask="phone">
                                </div>
                                <div class="input_container">
                                    <input type="text" placeholder="Ваша почта" name="e-mail" class="required"
                                        data-mask="email">
                                </div>
                            </div>

                            <div class="text-center">
                                <button class="button" type="submit">Оставить заявку</button>
                            </div>

                            <label class="form-agreement">
                                <input class="form-agreement__input required" type="checkbox" checked="checked"
                                    value="Согласие на обработку данных" name="Agreement">
                                <span class="form-agreement__text">
                                    <span class="form-agreement__check"></span>
                                    Я даю свое согласие на обработку персональных данных и соглашаюсь с <a
                                        href="">политикой конфиденциальности</a>
                                </span>
                            </label>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<? include $_SERVER['DOCUMENT_ROOT'].'/app/html/footer.php'?>
