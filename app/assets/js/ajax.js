$(function () {

	// отправка форм
	$(document).on("submit", "form", function(e){
		e.preventDefault();
		var name = $(this).attr("name");
		var form = $(this);
		var checkbox = form.find("input[type=checkbox]");

		if (form.valid()) {

			if(name === "main-form") {
				$.ajax({
					type: "POST",
					data: null,
					url: "/app/test.php",
					success: function(data) {
						console.log("Отправка завершена");
						console.log(data);
						clearForm(form);
					}
				})
			}

			if(name === "order-call") {
				$.ajax({
					type: "POST",
					data: null,
					url: "/app/test.php",
					success: function(data) {
						console.log("Отправка завершена");
						console.log(data);


						clearForm(form);
						// закрытие попапа
						setTimeout(function(){
							closePopup();
						},500)
					}
				})
			}


			if(name === "consultation") {
				$.ajax({
					type: "POST",
					data: form.serialize(),
					url: "/app/test.php",
					success: function(data) {
						console.log("Отправка завершена");
						console.log(data);


						clearForm(form);
						// закрытие попапа
						setTimeout(function(){
							closePopup();
						},500)
					}
				})
			}

			// вход в лк частное лицо
			if(name === "private-form") {
				$.ajax({
					type: "POST",
					data: form.serialize(),
					url: "/app/test.php",
					success: function(data) {
						console.log("Отправка завершена");
						console.log(data);


						clearForm(form);
					}
				})
			}

			// вход в лк юр лицо
			if(name === "legal-form") {
				$.ajax({
					type: "POST",
					data: form.serialize(),
					url: "/app/test.php",
					success: function(data) {
						console.log("Отправка завершена");
						console.log(data);


						clearForm(form);
					}
				})
			}

			// восстановление пароля
			if(name === "password-recovery") {
				var mail = form.find("input[name=email]").val();
				var parent = form.closest(".container");
				var succsesMessage = parent.find(".popup_custom__succses");

				$.ajax({
					type: "POST",
					data: form.serialize(),
					url: "/app/test.php",
					success: function(data) {
						console.log("Отправка завершена");
						console.log(data);

						parent.find("#recovery-mail").html(mail);
						setTimeout(function(){
							$(".popup_inner__wrap, .popup_custom__head p").hide(200);
							succsesMessage.show(200);
						}, 50);
						clearForm(form);
					}
				})
			}

			// регистрация частное лицо
			if(name === "private-reg") {
				$.ajax({
					type: "POST",
					data: form.serialize(),
					url: "/app/test.php",
					success: function(data) {
						console.log("Отправка завершена");
						console.log(data);


						clearForm(form);
					}
				})
			}

			// регистрация частное лицо
			if(name === "header-search-form") {
				$.ajax({
					type: "POST",
					data: form.serialize(),
					url: "/app/test.php",
					success: function(data) {
						console.log("Отправка завершена");
						console.log(data);


						setTimeout(function(){
							clearForm(form);
						}, 100)

						window.location.href = 'search.php';
					}
				})
			}

		}
	});

	// показать бренды в каталоге
	$(document).on("click", ".catalog-products__brands--show-more span", function(){
		var parent = $(this).closest(".catalog-products__col");
		var container = parent.find(".catalog-products__list.--brands-list");

		$.ajax({
			type: "POST",
			data: null,
			url: "/app/include/pages/brands-list.php",
			success: function(data) {
				container.append(data);
			}
		})

	});

	// подстановка текста и отправка данных сортировки в каталоге
	$(document).on("click", ".catalog-products__sort-item", function(){
		var parent = $(this).closest(".catalog-products__sort");
		var text = $(this).html();
		var sort = $(this).attr("data-sort");
		var catalogContainer = $("#catalog-container");

		$.ajax({
			type: "POST",
			data: {
				sort: sort,
			},
			url: "/app/include/ajax/content/catalog.php",
			success: function(data) {
				catalogContainer.html(data);

				$("html, body").animate({
					scrollTop: catalogContainer.offset().top - 50,
				},1000);

				parent.find(".catalog-products__sort-name").html(text);

				parent.find(".catalog-products__sort-list").fadeOut();

				parent.find(".catalog-products__sort-btn").removeClass("active");
			}
		})
	});

	// Показ категорий в каталоге
	$(document).on("click", ".catalog-products__item a, .catalog-products__subitem a", function(e){
		e.preventDefault();
		var $this = $(this);
		var category = $this.closest(".catalog-products__item").attr("data-cat");
		var catalogContainer = $("#catalog-container");

		$.ajax({
			type: "POST",
			data: {
				category: category,
			},
			url: "/app/include/ajax/content/catalog.php",
			success: function(data) {
				catalogContainer.html(data);

				$("html, body").animate({
					scrollTop: catalogContainer.offset().top - 50,
				},1000);

			}
		})
	});

	$(document).on("submit", ".catalog-products__search", function(e){
		e.preventDefault();
		var form = $(this);
		var parent = $(this).closest(".catalog-products__wrapper");
		var brandList = parent.find(".catalog-products__list.--brands-list");

		$.ajax({
			type: "POST",
			data: form.serialize(),
			url: form.attr("action"),
			success: function(data) {
				brandList.html(data);
				clearForm(form);
			}
		})

	})

	// показать еще на странице проекты
	$(document).on("click", ".cases__button-show", function(e){
		e.preventDefault();
		var $this = $(this);
		var container = $this.closest(".cases");
		var casesContainer = container.find("#cases__list");

		$.ajax({
			type: "POST",
			data: null,
			url: "/app/include/ajax/content/cases.php",
			success: function(data) {
				casesContainer.append(data);
			}
		})
	});

	// показать еще список партнеров на странице о компании
	$(document).on("click", ".clients__btn", function(e){
		e.preventDefault();
		var $this = $(this);
		var parent = $(this).closest(".clients__wrapper");

		if (window.matchMedia("(max-width: 993px)").matches) {
			if ($this.hasClass("_active")) {
				parent.removeClass("active");
				parent.find(".clients__col:nth-child(n+10)").remove();
				$this.removeClass("_active");
				$this.find("span").html("Посмотреть все");
			} else {
				parent.addClass("active");

				$.ajax({
					type: "POST",
					data: null,
					url: "/app/include/ajax/content/mobile-brand.php",
					success: function(data) {
						let brands = $(data);
						var item = brands.find(".clients__col");
						var itemLength = item.length

						parent.find(".clients__row").append(item)
						$this.addClass("_active");
						$this.find("span").html("Скрыть");
					}
				})
			}
		} else {

			if ($this.hasClass("_active")) {
				parent.removeClass("active");
				parent.find(".clients__col:nth-child(n+25)").remove();
				$this.removeClass("_active");
				$this.find("span").html("Посмотреть все");
			} else {
				parent.addClass("active");

				$.ajax({
					type: "POST",
					data: null,
					url: "/app/include/ajax/content/brands.php",
					success: function(data) {
						let brands = $(data);
						var item = brands.find(".clients__col");
						var itemLength = item.length

						parent.find(".clients__row").append(item)
						$this.addClass("_active");
						$this.find("span").html("Скрыть");
					}
				})
			}
		}

	});
});
