const options = BodyScrollOptions = {
	reserveScrollBarGap: true,
};

// Подгрузка html-контента в конец body
function loadPopup(popupId, onOpen) {
	var popup = $("#" + popupId);
	var data = $(this).data();

	if (popup.attr("data-remove-onclose")) {
		popup.parent().remove();
	}
	bodyScrollLock.clearAllBodyScrollLocks();

	if ($("#" + popupId).length) {
		popup.addClass("active");
		bodyScrollLock.disableBodyScroll(popup.find(".popup_custom__inner")[0], options);
		if (typeof onOpen === "function") onOpen();
	} else {
		return $.post(
			"/app/include/ajax/popup/" + popupId + ".php",
			data,
			function (data) {
				var data = $("<div>" + data + "</div>");
				var popup = $(data).find("#" + popupId);
				$("body").append(data);
				setTimeout(function () {
					popup.addClass("active");
					bodyScrollLock.disableBodyScroll(popup.find(".popup_custom__inner")[0], options);
				}, 30);

				initMasks();
			}
		);
	}
}

function closePopup() {
	var popup = $(".popup_custom");
	var form = popup.find("form");
	var popupId = popup.attr("id");
	popup.removeClass("active");
	if (!$(".header_main").hasClass("active")) {
		bodyScrollLock.enableBodyScroll(popup.find(".popup_custom__inner")[0]);
	}
	clearForm(form);
}
