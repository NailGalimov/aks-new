let flag = false;
// Проверка на ios
var iOS = /iPad|iPhone|iPod/.test(navigator.platform || "");
if (iOS) {
	$('body').addClass('ios');
}

// get параметр
var gets = (function () {
	var a = window.location.search;
	var b = new Object();
	a = a.substring(1).split("&");
	for (var i = 0; i < a.length; i++) {
		c = a[i].split("=");
		b[c[0]] = c[1]
	}
	return b
})();

function setViewport() {
    var mvp = $('#myViewport');
    if (window.matchMedia("(max-width: 360px)").matches) {
        mvp.attr('content', 'user-scalable=no, width=360');
    } else {
        mvp.attr('content', 'width=device-width');
    }
};

$(window).resize(function () {
    setViewport()
});

function initMasks() {
	// Настройки валидации
	$("form.validate").each(function () {
		$(this).validate();
	});
	$("form.validate .required").rules("add", {
		required: true,
	});
	$(document).on("blur", "form.validate input,select,textarea", function () {
		var el = $(this);
		var form = $(this).parents("form");
		if ($.data(form[0], "validator")) {
			el.valid();
		}
	});

	// Селекторы для полей
	var fio = $("[data-mask=fio]"), // ФИО
		phone = $("[data-mask=phone]"), // Телефон
		email = $("[data-mask=email]"), // Email
		letters = $("[data-mask=letters]"), // Только буквы
		digits = $("[data-mask=digits]"), // Только цифры
		decimals = $("[data-mask=decimals]"); // Только дробные

	// ФИО
	fio.formatFeild({
		titleCase: true,
		layout: "toRu",
	});
	fio.each(function () {
		$(this).rules("add", {
			minlength: 2,
			maxlength: 100,
		});
	});

	phone.mask("+7(999) 999-99-99");

	phone.each(function () {
		$(this).rules("add", {
			ruPhone: true,
		});
	});

	// Email
	email.formatFeild({
		layout: "toEn",
		noSpaces: true,
	});
	email.each(function () {
		$(this).rules("add", {
			email: true,
			maxlength: 100,
		});
	});

	// Проверка паролей
	$("[data-mask=samePass]").each(function(){
		$(this).rules("add", {
			equalTo: ".logInPass",
			minlength: 6
		});
	});
	$("[data-mask=samePass1]").each(function(){
		$(this).rules("add", {
			equalTo: ".logInPass1",
			minlength: 6
		});
	});

	// ИНН юр.лицо
	$("[data-mask=innUR]").mask('9999999999', {
		clearIncomplete: true,
		onincomplete: function () {
			var form = $(this).parents("form");
			if ($.data(form[0], 'validator')) {
				$(this).valid();
			}
		}
	})

	// КПП
	$("[data-mask=kpp]").mask("999999999", {
		clearIncomplete: true,
		onincomplete: function () {
			var form = $(this).parents("form");
			if ($.data(form[0], 'validator')) {
				$(this).valid();
			}
		}
	});
}

// Очистка формы
function clearForm($form) {
	$form = $("form");

	$form.each(function () {
		this.reset();
	});
	$form.find("input[type=file]").val("");
	$form.find(".valid").each(function () {
		$(this).removeClass("valid");
	});
	$form.find("label.error").remove();
	$form.find(".error").each(function () {
		$(this).removeClass("error");
	});
	$form.find("input").removeAttr("disabled");

	// select reset
	$form.find(".select").each(function () {
		var $th = $(this);
		var $selectItems = $th.find(".select__item");
		var defaultValue =
			$selectItems.eq(0).attr("data-value") || $selectItems.eq(0).html();
		var defaultText = $selectItems.eq(0).html();

		$th.find(".select__input").val(defaultValue);
		$th.find(".select__val").html(defaultText);
	});
}

// анимация карты
function mapAnimation() {
	var map = $(".map");
	var tl = gsap.timeline({
		scrollTrigger: {
			animation: tl,
			trigger: ".worlwide",
		}
	});

	// анимация заголовка
	tl.to(".worlwide .main-title", {x:0, opacity: 1, duration: 0.5});

	if($(".first-line__dot-start").length) {
		tl.to("#RU-KHM_1_", {fill: "#c8d5de", duration: 0.7});
		tl.to(".first-line__dot-start", {opacity: 1, duration: 0.5});
		tl.to(".first-line", {strokeDashoffset: 0, duration: 0.5});
		tl.to(".first-line__dot-end", {opacity: 1, duration: 0.5});
		tl.to("#RU-KYA_1_", {fill: "#c8d5de", duration: 0.7});

		tl.to("#RU-VLA_1_", {fill: "#c8d5de", duration: 0.7});
		tl.to(".second-line__dot-start", {opacity: 1, duration: 0.5});
		tl.to(".second-line", {strokeDashoffset: 0, duration: 0.5});
		tl.to(".second-line__dot-end", {opacity: 1, duration: 0.5});
		tl.to("#RU-PER_1_", {fill: "#c8d5de", duration: 0.7});

		tl.to("#RU-KO_1_", {fill: "#c8d5de", duration: 0.7});
		tl.to(".third-line__dot-start", {opacity: 1, duration: 0.5});
		tl.to(".third-line", {strokeDashoffset: 0, duration: 0.5});
		tl.to(".third-line__dot-end", {opacity: 1, duration: 0.5});
		tl.to("#RU-OMS_1_", {fill: "#c8d5de", duration: 0.7});
	}
}

// инициализация карты
var maps = [];
function initMaps() {
	if ($('.ya_map').length) {
		$('.ya_map').each(function () {
			if ($(this).attr('id')) {
				var id = $(this).attr('id');
				var dataCenter = $(this).attr('data-center');
				var coords = [50.590376, 36.626362];
				var markers = $(this).attr('data-markers');
				var zoom = $(this).attr('data-zoom') || 12;

				if (markers) {
					markers = JSON.parse(markers);
				}

				if (dataCenter) {

					// Центрируем по координатам
					coords = dataCenter.split(',');

				}

				// Создание карты.
				var myMap = new ymaps.Map(id, {
					center: coords,
					zoom: zoom,
					controls: []
				});

				// Метка
				if (markers) {
					$(markers).each(function (idx, el) {
						addPlacemark(el);
					});
				} else {
					addPlacemark(coords);
				}

				// Добавление меток
				function addPlacemark(coords, onClick) {
					var myPlacemark = new ymaps.Placemark(coords, {}, {
						iconLayout: 'default#image',
						iconImageHref: '/app/assets/images/icon-marker.svg',
						iconImageSize: [20, 28],
						iconImageOffset: [-10, -14]
					});

					myMap.geoObjects
						.add(myPlacemark)

				}

				// Сохранение карты в массив
				maps.push({
					id: id,
					map: myMap,
					setCenter: function (coords, zoom) {
						myMap.setCenter(coords, zoom, {
							duration: 500,
							timingFunction: "ease-out"
						});
					},
					addPlacemark: function (coords) {
						addPlacemark(coords);
					},
					removePlacemarks: function () {
						myMap.geoObjects.removeAll()
					}
				});
			}
		});
	}
}

// yandex maps
if ($('.ya_map').length) {
	var tag = document.createElement('script');
	tag.src = "https://api-maps.yandex.ru/2.1/?apikey=adaff80d-a56b-4e67-a7af-a758f076e71b&lang=ru_RU&onload=initMaps";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

$(function () {
	setViewport();
	mapAnimation();
	initMasks();

	// Валидация номера телефона
	$.validator.addMethod(
		"ruPhone",
		function (value, element) {
			return /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/.test(
				value
			);
		},
		"Некорректный номер"
	);

	// Проверка паролей
	$.validator.addMethod("equalTo", function (value, element, message) {
		var isValid = false;
		var form = $(element).closest("form");
		if ($(message).val() != "") {
			if ($(element).val() === form.find(message).val()) {
				isValid = true;
			} else {
				isValid = false;
			}
		} else {
			isValid = true;
		}


		return (this.optional(element) || isValid);
	}, "Пароли не совпадают");

	// Открытие попапов
	$(document).on("click", "[data-popup]", function (e) {
		e.preventDefault();
		var popupId = $(this).attr("data-popup");
		var data = $(this).data();
		var onOpen;

		loadPopup.call(this, popupId, onOpen);
	});

	// Закрытие попапа
	$(document).on("click", ".popup_close, .popupClose", function () {
		var popup = $(this).closest(".popup_custom");
		var form = popup.find("form");
		var popupId = popup.attr("id");
		popup.removeClass("active");

		if (!$(".header_main").hasClass("active")) {
			bodyScrollLock.enableBodyScroll(popup.find(".popup_custom__inner")[0]);
		}

		if (popup.find(".button").hasClass("popup_custom__succses-button")) {
			setTimeout(function(){
				popup.find(".popup_inner__wrap, .popup_custom__head p").show();
				popup.find(".popup_custom__succses").hide();
			}, 300)
		}

		clearForm(form);
	});
	// закрытие попапа вне окна
	$(document).on("click", function (e) {
		if (!$(e.target).closest(".menu__inner, .popup_custom__inner, [data-popup]").length) {
			var popup = $(e.target).closest(".popup_custom");
			var form = popup.find("form");
			var popupId = popup.attr("id");
			if (popup.length) {
				popup.removeClass("active");
				if (!$(".header_main").hasClass("active")) {
					bodyScrollLock.enableBodyScroll(popup.find(".popup_custom__inner")[0]);
				}
				if (form.is("[name=password-recovery]")) {
					setTimeout(function(){
						popup.find(".popup_inner__wrap, .popup_custom__head p").show();
						popup.find(".popup_custom__succses").hide();
					}, 300)
				}
				clearForm(form);
			}
		}
	});


	// анимация счетчика
	$(".countAnimation").each(function(){
		var $this = $(this);
        var controller = new ScrollMagic.Controller();
        var scene = new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: 'onEnter'
            })
            .on('start', function () {
                countUp($this);
            })
            .addTo(controller);
	});
	function countUp($el) {
		$({
            Counter: 0
        }).animate({
            Counter: $el.attr('data-text')
        }, {
            duration: 2000,
            easing: 'swing',
            step: function () {
                $el.text(Math.ceil(this.Counter));
            }
        });
	}

	// слайдеры
	$(".portfolio__slider").slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		infinite: false,
		appendArrows: ".portfolio__arrow-container",
		nextArrow: "<div class='arrow next'><span class='icon-right-arrow arrow-right '></div>",
		prevArrow: "<div class='arrow prev'><span class='icon-right-arrow arrow-right left'></span></div>",
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 740,
				settings: {
					slidesToShow: 1,
				}
			},
		]
	})
	$(".cooperation__slider").slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		appendArrows: ".cooperation__arrow-container",
		nextArrow: "<div class='arrow next'><span class='icon-right-arrow arrow-right'></div>",
		prevArrow: "<div class='arrow prev'><span class='icon-right-arrow arrow-right left'></span></div>",
		responsive: [
			{
				breakpoint: 740,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 490,
				settings: {
					slidesToShow: 1,
					infinite: false,
				}
			},
		]
	})


	// плавный скрол по якорю
	$(document).on("click", 'a[href^="#"]', function (event) {
		event.preventDefault();

		$("html, body").animate(
			{
				scrollTop: $($.attr(this, "href")).offset().top,
			},
			1000
		);
	});

	function isReversed(animation) {
		animation.reversed() ? animation.play() : animation.reverse();
	}

	let menuTimeLine = gsap.timeline({
		paused: true,
		reversed: true
	})
	// открытие меню
	ScrollTrigger.matchMedia({
		// для десктопа
		"(min-width: 1025px)": function() {

			menuTimeLine
				.to(".header_main__info", {
					duration: 0.7,
					x: 0,
					ease: Power4.easeInOut
				})
				.staggerTo(".header_main__nav ul li", 0.45,{
					x: 0,
					opacity: 1,
				}, 0.1, "-=0.3")
				.staggerTo(".animElMenu", 0.45,{
					x: 0,
					opacity: 1,
				}, 0.1, "-=0.7")
				.to(".b2b-copy", {
					y: 0,
					opacity: 1,
					duration: 0.3
				}, "-=0.2")
				.to(".header_main__policy", {
					y: 0,
					opacity: 1,
					duration: 0.3,
					onComplete: function(){
						$(".js--burger").css("pointer-events", "auto");
					}
				}, "-=0.2");

			$(document).on("click", ".js--burger", function(e){
				var parent = $(this).closest(".header_main");
				parent.toggleClass("active");

				$(this).find(".menu_burger_main").toggleClass("active");

				if (menuTimeLine.isActive()) {
					e.preventDefault();
					return false;
				}

				isReversed(menuTimeLine);

				switch (flag) {
					case false:
						bodyScrollLock.disableBodyScroll(document.querySelector(".header_main__row"), options);
						$(".js--burger").css("pointer-events", "none");
						flag = true;
						break;
					case true:
						bodyScrollLock.clearAllBodyScrollLocks();
						$(".js--burger").css("pointer-events", "auto");
						flag = false;
						break;
				}

				console.log(flag);

				// закрытие пунктов меню
				if($(".basket-cart").hasClass("active")) {
					baskeTl.reverse();
					bodyScrollLock.disableBodyScroll(document.querySelector(".header_main__row"), options);
					flag = true;
					$(".basket-cart").removeClass("active");
				}
				if ($(".js--user-cabinet").hasClass("active")) {
					cabTl.reverse();
					bodyScrollLock.disableBodyScroll(document.querySelector(".header_main__row"), options);
					flag = true;
					$(".js--user-cabinet").removeClass("active");
				}

			});
		},
		// для планшета и мобильных
		"(max-width: 1025px)": function() {
			menuTimeLine
				.to(".mobile_burger", {
					right: 0,
					duration: 0.1,
					opacity: 0
				})
				.to(".header_main", {
					duration: 0.7,
					x: 0,
					ease: Power4.easeInOut,
				}, "-=0.2")
				.staggerTo(".header_main__nav ul li", 0.45,{
					x: 0,
					opacity: 1,
				}, 0.1, "-=0.3")
				.staggerTo(".animElMenu", 0.45,{
					x: 0,
					opacity: 1,
				}, 0.1, "-=0.7")
				.to(".b2b-copy", {
					y: 0,
					opacity: 1,
					duration: 0.3
				}, "-=0.2")
				.to(".header_main__policy", {
					y: 0,
					opacity: 1,
					duration: 0.3,
				}, "-=0.2");

			$(document).on("click", ".js--burger", function(e){
				var parent = $(this).closest(".header_main");
				parent.toggleClass("active");

				parent.find(".menu_burger_main").toggleClass("active");

				if (menuTimeLine.isActive()) {
					e.preventDefault();
					return false;
				}

				isReversed(menuTimeLine);

				switch (flag) {
					case false:
						bodyScrollLock.disableBodyScroll(document.querySelector(".header_main__row"), options);
						flag = true;
						break;
					case true:
						bodyScrollLock.clearAllBodyScrollLocks();
						flag = false;
						baskeTl.reverse();
						cabTl.reverse();
						break;
				}

				// закрытие пунктов меню
				if($(".basket-cart").hasClass("active")) {
					baskeTl.reverse();
					bodyScrollLock.clearAllBodyScrollLocks();
					flag = true;
					$(".basket-cart").removeClass("active");
				}
				if ($(".js--user-cabinet").hasClass("active")) {
					cabTl.reverse();
					bodyScrollLock.clearAllBodyScrollLocks();
					flag = true;
					$(".js--user-cabinet").removeClass("active");
				}
			});
		},
	});



	// скрытие меню при скролле в млбилке
	var lastScrollTop = 0;
	window.addEventListener("scroll", function(){
		var scroll = window.pageYOffset || document.documentElement.scrollTop;

		if(scroll >= 100) {
			$(".mobile_burger").css("top", "1.3rem");
			$(".mobile_burger").removeClass("hidden");

			if (scroll < lastScrollTop){
				$(".mobile_burger").addClass("hidden");
			}
		} else {
			$(".mobile_burger").addClass("hidden");
		}

		lastScrollTop = scroll;
	}, false);


	// анимация заголовков
	var mainTl = gsap.timeline();
	var heroTitle = $(".hero_section .main-title"),
		ourProductsTitle = $(".our_products .main-title"),
		solutionsTitle = $(".solutions .main-title"),
		portfolioTitle = $(".portfolio .main-title"),
		footerSectionTitle = $(".footer_section .main-title"),
		cooperationTitle = $(".cooperation .main-title");
	var heroBtn = $(".btn_wrap");

	if($(".hero_section").length) {
		mainTl.to(heroTitle, {x: 0,opacity: 1,scrollTrigger: {trigger: '.hero_section',}})
	}

	if($(".hero_section__img").length) {
		mainTl.to(".hero_section__img", {opacity: 1,});
	}

	if(heroBtn.length) {
		mainTl.from(heroBtn, {y: "100%", opacity: 0});
	}

	if(ourProductsTitle.length) {
		gsap.to(ourProductsTitle, {x: 0,opacity: 1,scrollTrigger: {trigger: '.our_products',}})
	}

	if(solutionsTitle.length) {
		gsap.to(solutionsTitle, {x: 0,opacity: 1,scrollTrigger: {trigger: '.solutions',}})
	}

	if(portfolioTitle.length) {
		gsap.to(portfolioTitle, {x: 0,opacity: 1,scrollTrigger: {trigger: '.portfolio',}})
	}

	if(cooperationTitle.length) {
		gsap.to(cooperationTitle, {x: 0,opacity: 1,scrollTrigger: {trigger: '.cooperation',}})
	}

	if(footerSectionTitle.length) {
		gsap.to(footerSectionTitle, {x: 0,opacity: 1,scrollTrigger: {trigger: '.footer_section',}})
	}

	// анимация второго экрана page
	var ballTimeLine = gsap.timeline({
		scrollTrigger: {
			trigger: '.js--air-ball-trigger',
			scrub: true,
			scrub: 0.5,
			start: "top bottom"
		}
	})

	ballTimeLine.fromTo(".js--air-ball", {
		y: "310%"
	},
	{
		y: 0,
		duration: 2,
	})

	// анимация второго экрана page2
	var secTwoTimeLine = gsap.timeline({
		scrollTrigger: {
			trigger: '.js--secTwo',
			start: "10% bottom",
		}
	})

	secTwoTimeLine.fromTo(".js--secTwoImage", {
		x: "-1000%"
	}, {
		x: 0,
		duration: 3,
	})


	// читать далее текст
	$(document).on("click", ".js--read", function(e){
		e.preventDefault();
		var parent = $(this).closest(".worlwide__text");

		parent.toggleClass("active");
	});

	// показать сабменю в каталоге
	$(document).on("click", ".js--show-submenu", function(){
		var parent = $(this).closest(".catalog-products__item");

		$(this).toggleClass("active");

		parent.find(".catalog-products__sublist").fadeToggle();
	});

	// показать сортировку ы в каталоге
	$(document).on("click", ".catalog-products__sort-btn", function(){
		var parent = $(this).closest(".catalog-products__sort");

		$(this).toggleClass("active");

		$(".catalog-products__sort-list").fadeToggle();
	});

	// rangeslider на странице каталога
	var rangeSlider = document.getElementById("range-slider");

	if (rangeSlider) {
		noUiSlider.create(rangeSlider, {
			start: [500, 199999],
			connect: true,
			step: 1,
			range: {
				"min": [500],
				"max": [199999]
			}
		});

		var input0 = document.getElementById("input0");
		var input1 = document.getElementById("input1");
		var inputs = [input0, input1];

		rangeSlider.noUiSlider.on('update', function(values, handle){
			inputs[handle].value = Math.round(values[handle]);
		});

		rangeSlider.noUiSlider.on('change', function(values, handle){
			inputs[handle].value = Math.round(values[handle]);
			var catalogContainer = $("#catalog-container");

			$.ajax({
				type: "POST",
				data: {
					name: handle,
					price: values[handle]
				},
				url: "/app/include/ajax/content/catalog.php",
				success: function(data) {
					catalogContainer.html(data);

					console.log(catalogContainer);

					$("html, body").animate({
						scrollTop: catalogContainer.offset().top - 150,
					},1000);
				}
			})

		});

		const setRangeSlider = (i, value) => {
			let arr = [null, null];
			arr[i] = value;

			console.log(arr);

			rangeSlider.noUiSlider.set(arr);
		};

		inputs.forEach((el, index) => {
			el.addEventListener('change', (e) => {
				console.log(index);
				setRangeSlider(index, e.currentTarget.value);
				let val = e.currentTarget.value;

				console.log(val);
				var catalogContainer = $("#catalog-container");

				$.ajax({
					type: "POST",
					data: {
						name: index,
						price: val
					},
					url: "/app/include/ajax/content/catalog.php",
					success: function(data) {
						catalogContainer.html(data);

						$("html, body").animate({
							scrollTop: catalogContainer.offset().top - 150,
						},1000);
					}
				})
			});
		});
	}

	// открытие фильтра в каталоге в мобилке
	if (window.matchMedia("(max-width: 740px)").matches) {
		$(document).on("click", ".catalog-products__legend", function(){
			var attr = $(this).attr("data-parent");

			$(this).toggleClass("active");

			$(`.catalog-products__wrapper[data-child="${attr}"]`).fadeToggle();
		});
	}

	// слайдер картинок в карточке товара
	$(".cart__images--big").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: false,
		asNavFor: '.cart__images--small'
	})
	$(".cart__images--small").slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		asNavFor: '.cart__images--big',
		vertical: true,
  		focusOnSelect: true
	})

	// Табы
	$(document).on("click", ".tabs [data-show]:not(.active)", function () {
		var container = $(this).closest(".tabs"),
			toShow = $(this).attr("data-show") || 1;
			activeTab = container.find('[data-item="' + toShow + '"]');

		container.find("[data-show]").removeClass("active");
		$(this).addClass("active");

		container.find("[data-item]").removeClass("active");
		activeTab.addClass("active");

		container.find("[data-item]").hide();
		container.find('[data-item="' + toShow + '"]').fadeIn(500, function () {

		});

		container.find('.tabs [data-item]').hide();
		container.find('.tabs [data-item]:not(.hidden)').eq(0).show();
	});

	// слайдер популярных товаров в карточке товара
	if (window.matchMedia("(max-width: 1200px)").matches) {
		$(".cart__popular-list").slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			autoplay: true,
			speed: 900,
			autoplaySpeed: 1300,
			responsive: [
				{
					breakpoint: 740,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		})
	}

	$(document).on("click", ".basket-mini__delete", function(e){
		e.preventDefault();
	});

	// открытие мини-корзины
	let baskeTl = gsap.timeline({
		paused: true,
		reversed: true
	})
	baskeTl.to(".basket-mini", {
		x: 0,
		duration: 0.7,
		ease: Power4.easeInOut
	})

	ScrollTrigger.matchMedia({
		"(min-width: 1025px)": function() {
			$(document).on("click", ".basket-cart", function(e){
				e.preventDefault();

				$(this).toggleClass("active");

				if (baskeTl.isActive()) {
					e.preventDefault();
					return false;
				}

				isReversed(baskeTl);

				switch (flag) {
					case false:
						bodyScrollLock.disableBodyScroll(document.querySelector(".basket-mini__list"), options);
						flag = true;
						break;
					case true:
						bodyScrollLock.clearAllBodyScrollLocks();
						flag = false;
						break;
				}

				console.log(flag);

				if($(".header_main").hasClass("active")) {
					menuTimeLine.reverse();
					bodyScrollLock.disableBodyScroll(document.querySelector(".basket-mini__list"), options);
					flag = true;
					$(".header_main").removeClass("active");
					$(".menu_burger").removeClass("active");
				}

				if ($(".js--user-cabinet").hasClass("active")) {
					cabTl.reverse();
					bodyScrollLock.disableBodyScroll(document.querySelector(".basket-mini__list"), options);
					flag = true;
					$(".js--user-cabinet").removeClass("active");
				}
			});
		},
		"(max-width: 1025px)": function() {
			$(document).on("click", ".basket-cart", function(e){
				e.preventDefault();

				$(this).toggleClass("active");

				if (baskeTl.isActive()) {
					e.preventDefault();
					return false;
				}

				isReversed(baskeTl);

				switch (flag) {
					case false:
						bodyScrollLock.disableBodyScroll(document.querySelector(".basket-mini__list"), options);
						flag = true;
						break;
					case true:
						bodyScrollLock.clearAllBodyScrollLocks();
						flag = false;
						break;
				}

				if($(".header_main").hasClass("active")) {
					bodyScrollLock.disableBodyScroll(document.querySelector(".basket-mini__list"), options);
					flag = true;
					// $(".header_main").removeClass("active");
					// $(".menu_burger").removeClass("active");
				}
				if ($(".js--user-cabinet").hasClass("active")) {
					cabTl.reverse();
					bodyScrollLock.disableBodyScroll(document.querySelector(".basket-mini__list"), options);
					flag = true;
					$(".js--user-cabinet").removeClass("active");
				}
			});
		}
	});
	// открытие лк
	let cabTl = gsap.timeline({
		paused: true,
		reversed: true
	})
	cabTl.to(".cabinet", {
		x: 0,
		duration: 0.7,
		ease: Power4.easeInOut
	})

	ScrollTrigger.matchMedia({
		"(min-width: 1025px)": function() {
			$(document).on("click", ".js--user-cabinet", function(e){
				e.preventDefault();

				$(this).toggleClass("active");

				if (cabTl.isActive()) {
					e.preventDefault();
					return false;
				}

				isReversed(cabTl);

				switch (flag) {
					case false:
						bodyScrollLock.disableBodyScroll(document.querySelector(".cabinet__content"), options);
						flag = true;
						break;
					case true:
						bodyScrollLock.clearAllBodyScrollLocks();
						flag = false;
						break;
				}

				if($(".header_main").hasClass("active")) {
					menuTimeLine.reverse();
					bodyScrollLock.disableBodyScroll(document.querySelector(".cabinet__content"), options);
					flag = true;
					$(".header_main").removeClass("active");
					$(".menu_burger").removeClass("active");
				}

				if ($(".basket-cart").hasClass("active")) {
					baskeTl.reverse();
					bodyScrollLock.disableBodyScroll(document.querySelector(".cabinet__content"), options);
					flag = true;
					$(".basket-cart").removeClass("active");
				}
			});
		},
		"(max-width: 1025px)": function() {
			$(document).on("click", ".js--user-cabinet", function(e){
				e.preventDefault();

				$(this).toggleClass("active");

				if (cabTl.isActive()) {
					e.preventDefault();
					return false;
				}

				isReversed(cabTl);

				switch (flag) {
					case false:
						bodyScrollLock.disableBodyScroll(document.querySelector(".cabinet__content:hidden"), options);
						flag = true;
						break;
					case true:
						bodyScrollLock.clearAllBodyScrollLocks();
						flag = false;
						break;
				}


				if($(".header_main").hasClass("active")) {
					bodyScrollLock.disableBodyScroll(document.querySelector(".cabinet__content:hidden"), options);
					flag = true;
				}

				if ($(".basket-cart").hasClass("active")) {
					baskeTl.reverse();
					bodyScrollLock.disableBodyScroll(document.querySelector(".cabinet__content:hidden"), options);
					flag = true;
					$(".basket-cart").removeClass("active");
				}
			});
		}
	});

	$(document).on("click", function (e) {
		let $el = $(".basket-cart");
		let $el1 = $(".basket-mini");
		let $el2 = $(".header-navigation");
		let $el3 = $(".js--user-cabinet");
		let $el4 = $(".cabinet");

		if (
				!$el.is(e.target) && $el.has(e.target).length === 0
				&& !$el1.is(e.target) && $el1.has(e.target).length === 0
				&& !$el2.is(e.target) && $el2.has(e.target).length === 0
				&& !$el3.is(e.target) && $el3.has(e.target).length === 0
				&& !$el4.is(e.target) && $el4.has(e.target).length === 0
				&& !$(".header_main__info").is(e.target)
				&& $(".header_main__info").has(e.target).length === 0
				&& !$(e.target).closest(".menu__inner, .popup_custom__inner, [data-popup]").length)
			{
				baskeTl.reverse();
				cabTl.reverse();
				bodyScrollLock.clearAllBodyScrollLocks();
				flag = false;
				// console.log(flag);
				$el.removeClass("active");
		}
	});

	// клик по иконке показать пароль
	$(document).on("click", ".js-show-pass", function(e){
		e.preventDefault();

		$(this).toggleClass("active");

		var input = $(this).closest(".cabinet__input-container").find(".cabinet__input");

		input.attr("type", function(idx, attr){
			return attr == "password" ? "text" : "password";
		});
	});

	// переключение менжду входом в лк и регистрацией
	$(document).on("click", ".js--change-cab", function(e){
		e.preventDefault();

		var attr = $(this).attr("data-cab-show");

		$("[data-cab-item]").hide();

		$(`[data-cab-item="${attr}"]`).fadeIn(500, null);
	});

	var stepperInput = $(".quant__input");

	// кол-во товаров в карточке
	function changeQuant(e, op) {
		var parent = $(e.target).closest(".quant");
		var id = parent.attr('data-id');
		var textEl = parent.find(".quant__input");

		console.log(e.target);

		if (op === "plus") {
			var quant = parseInt(textEl.val()) + 1;
		} else if (op === "minus") {
			var quant = parseInt(textEl.val()) - 1;
		}

		var maxNumber = textEl.data("max-number");
		if (quant > maxNumber) return false;

		var minNumber = textEl.data("min-number");
		if (quant < minNumber) return false;

		if (quant == 1) {
			parent.find(".quant__btn--minus").removeClass("--disabled");
		} else {
			parent.find(".quant__btn--minus").addClass("--disabled");
		}

		if (quant >= 1) {
			textEl.val(quant);

			if(iOS) {
				textEl.css("width", `${textEl.val().length + 2}ex`)
			} else {
				textEl.css("width", `${textEl.val().length + 1}ex`)
			}

			parent.find(".quant__btn--minus").removeClass("--disabled");
			$('.quant[data-id="' + id + '"]').find('.quant__input').val(quant);

			parent.closest('.cart__info').find('.cart__button').attr('data-count', quant);

			// quantAjax(id, quant);
		}
	}

	$(document)
		.on("click", ".quant__btn--plus", function (e) {
			changeQuant(e, "plus");
		}).on("click", ".quant__btn--minus", function (e) {
			changeQuant(e, "minus");
		});

	stepperInput.on('keyup', function(e) {
		var self = e.currentTarget;
		var parent = $(self).closest(".quant");

		if (self.value == "0" || self.value == "") {
			self.value = 1;
		}

		if (iOS) {
			self.style.width = `${self.value.length + 2}ex`;
		} else {
			self.style.width = `${self.value.length + 1}ex`;
		}

		count = parent.find(".quant__input").value;

		if (count == 1) {
			parent.find(".quant__btn--minus").addClass('--disabled');
		} else {
			parent.find(".quant__btn--minus").removeClass('--disabled');
		}
	});

	// подстановка адреса на карту на странице оформления
	var timer;
	$(document).on('input', '.inputMap', function () {
		var $th = $(this);

		clearTimeout(timer);
		timer = setTimeout(function () {
			var parent = $th.closest('.mapChanger');

			var city = parent.find('.inputMap-city').val();

			var street = parent.find('.inputMap-street').val();
			if (street) street = ', ' + 'улица ' + street;

			var house = parent.find('.inputMap-house').val();
			if (house) house = ', ' + house;

			var adress = city + street + house;

			var mapId = parent.find('.ordering-page__map').attr('id');

			var myGeocoder = ymaps.geocode(adress);
			myGeocoder.then(
				function (res) {
					var center = res.geoObjects.get(0).geometry.getCoordinates();

					$(maps).each(function (idx, map) {
						if (map.id == mapId) {
							map.setCenter(center, 16);
							map.removePlacemarks();
							map.addPlacemark(center);
						}
					});
				},
				function (err) {

				}
			);
		}, 1000);
	});


	// показать все категории на главной странице
	$(document).on("click", ".solutions--show-el", function(e){
		e.preventDefault();
		var parent = $(this).closest(".solutions__list");

		parent.addClass("active");

		$(this).hide();
	})
});
