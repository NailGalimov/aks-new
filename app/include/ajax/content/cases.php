<?
	$arr = ["Аквапарк «Лазурный2»", "Токаревская птицефабрика2"];
?>

<? foreach ($arr as $val) {?>

	<li class="cases__item">
		<div class="cases__item--left">
			<h3 class="cases__item-title">
				<? echo $val; ?>
			</h3>

			<p class="cases__item-subtitle">
				п. Разумное, Белгородская область <span>2019г</span>
			</p>

			<article class="cases__item-text">
				<span class="cases__item-text-legend">
					Задача:
				</span>

				<p class="cases__item-text-info">
					поставка трубопровода и&nbsp;запорной арматуры на&nbsp;крупномасштабный объект Белгородской
					области. Наряду с&nbsp;подбором качественного материала важны были сроки поставки.
				</p>

				<span class="cases__item-text-legend">
					Результат:
				</span>

				<p class="cases__item-text-info">
					Краны, задвижки и&nbsp;затворы европейских брендов, поставленные на&nbsp;объект, отличаются
					своим качеством и&nbsp;гарантийным периодом эксплуатации.
				</p>
			</article>

			<span class="cases__item-price-title">
				Стоимость:
			</span>
			<div class="cases__item-cost">
				<span class="cases__item-cost-price">
					1,6 млрд рублей
				</span>

				<span class="cases__item-cost-text">
					Инвестиции в проект
				</span>
			</div>

		</div>
		<div class="cases__item--right">
			<div class="cases__item-img">
				<img src="../assets/images/pages/projects/projects-img1.jpg" alt="Аквапарк «Лазурный»">
			</div>
		</div>
	</li>

<? } ?>
