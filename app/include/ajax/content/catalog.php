<? for ($i=1; $i < 7; $i++) { ?>

	<div class="main_list__col catalog-products__container-col">
		<a href="" class="main_list__item catalog-products__container-item" style="background-image: url(../assets/images/pages/catalog/product_img4.jpg)">
			<div class="main_list__item_text">
				<p class="main_list__text">Запорная арматура</p>
			</div>

			<div class="our_products__item_btn item_btn">
				<span class="item_btn--hover" data-text-2="Подоробнее" data-text="ОТ 17 000 РУБЛЕЙ" >Подробнее</span>
				<div class="item_btn__icon">
					<span class="icon-right-arrow arrow-right"></span>
					<svg xmlns="http://www.w3.org/2000/svg" width="14.618" height="22.981" viewBox="0 0 14.618 22.981">
						<path id="icon-arrow" d="M10.378,7.672,17.959.246a.866.866,0,0,1,1.216.007L20.5,1.581a.866.866,0,0,1,0,1.226l-9.51,9.45a.865.865,0,0,1-1.223,0L.256,2.807a.866.866,0,0,1,0-1.226L1.581.253A.866.866,0,0,1,2.8.246Z" transform="translate(1.108 21.868) rotate(-90)" fill="#fff" stroke="#d0241f" stroke-width="2"/>
					</svg>
				</div>
			</div>
		</a>
	</div>

<? } ?>
