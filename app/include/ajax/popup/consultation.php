<div class="popup_custom" id="consultation">
	<div class="popup_custom__overlay"></div>
	<div class="popup_custom__inner">
		<div class="popup_close popup_custom_close icon-cross"></div>
		<div class="popup_custom__scroll">
			<div class="container">
				<div class="popup_custom__head">
					<div class="popup_custom__title">Получить консультацию</div>
				</div>
				<div class="popup_inner__wrap">
					<form class="form formValidate validate" name="consultation">
						<div class="input_wrapper">
							<div class="input_container">
								<input type="text" placeholder="Ваше имя" name="consultation_name" class="required" data-mask="fio">
							</div>
							<div class="input_container">
								<input type="text" placeholder="Ваш номер" name="consultation_tell" class="required" data-mask="phone">
							</div>
						</div>

						<button class="button" type="submit">Получить консультацию</button>

						<label class="form-agreement">
							<input class="form-agreement__input required" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
							<span class="form-agreement__text">
								<span class="form-agreement__check"></span>
								Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
							</span>
						</label>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
