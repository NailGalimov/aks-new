<div class="popup_custom" id="forgot-password">
	<div class="popup_custom__overlay"></div>
	<div class="popup_custom__inner">
		<div class="popup_close popup_custom_close icon-cross"></div>
		<div class="popup_custom__scroll">
			<div class="container">
				<div class="popup_custom__head">
					<div class="popup_custom__title">Восстановление пароля</div>
					<p>Для восстановления пароля, введите ваш электронный почтовый адрес, который был указан при регистрации аккаунта</p>
				</div>
				<div class="popup_inner__wrap">
					<form class="form formValidate validate" name="password-recovery">
						<div class="input_wrapper">
							<div class="input_container">
								<input type="text" placeholder="Ваш email" name="email" class="required" data-mask="email">
							</div>
						</div>

						<button class="button" type="submit">Получить пароль</button>
					</form>
				</div>
				<div class="popup_custom__succses">
					<p class="popup_custom__succses-text">
						На почту <span id="recovery-mail">mail@mail.ru</span> была отправлена ссылка для восстановления пароля. Если ссылка не пришла, попробуйте еще раз.
					</p>
					<button class="button popup_custom__succses-button popupClose">Закрыть</button>
				</div>
			</div>
		</div>
	</div>
</div>
