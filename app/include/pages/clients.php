<!-- descktop row -->
<div class="clients__row">
	<div class="clients__col"></div>
	<div class="clients__col clients__col_item"><img src="../assets/images/0.png" alt=""></div>
	<div class="clients__col"></div>
	<div class="clients__col clients__col_item"><img src="../assets/images/2.png" alt=""></div>
	<div class="clients__col"></div>
	<div class="clients__col clients__col_item clients__col_item--red"><img src="../assets/images/4.png" alt=""></div>
	<div class="clients__col"></div>
	<div class="clients__col"></div>
	<div class="clients__col"></div>
	<div class="clients__col"></div>
	<div class="clients__col clients__col_item clients__col_item--red"><img src="../assets/images/7.png" alt=""></div>
	<div class="clients__col"></div>
	<div class="clients__col clients__col_item"><img src="../assets/images/9.png" alt=""></div>
	<div class="clients__col"></div>
	<div class="clients__col clients__col_item"><img src="../assets/images/11.png" alt=""></div>
	<div class="clients__col"></div>
	<div class="clients__col"></div>
	<div class="clients__col clients__img-show"><img src="../assets/images/12.png" alt=""></div>
	<div class="clients__col clients__img-show"><img src="../assets/images/13.png" alt=""></div>
	<div class="clients__col clients__img-show"><img src="../assets/images/14.png" alt=""></div>
	<div class="clients__col clients__img-show"><img src="../assets/images/15.png" alt=""></div>
	<div class="clients__col clients__img-show"><img src="../assets/images/16.png" alt=""></div>
	<div class="clients__col clients__img-show"><img src="../assets/images/17.png" alt=""></div>
	<div class="clients__col"></div>
</div>
<!-- descktop row end -->

<!-- mobile row -->
<div class="clients__row clients__row--mobile">
	<div class="clients__col clients__col--mobile clients__col_item--mobile"><img src="../assets/images/0.png" alt="Лого клиента"></div>
	<div class="clients__col clients__col--mobile clients__col_item--mobile"><img src="../assets/images/1.png" alt="Лого клиента"></div>
	<div class="clients__col clients__col--mobile clients__col_item--mobile"><img src="../assets/images/2.png" alt="Лого клиента"></div>
	<div class="clients__col clients__col--mobile clients__col_item--mobile"><img src="../assets/images/3.png" alt="Лого клиента"></div>
	<div class="clients__col clients__col--mobile clients__col_item--mobile"><img src="../assets/images/4.png" alt="Лого клиента"></div>
	<div class="clients__col clients__col--mobile clients__col_item--mobile"><img src="../assets/images/5.png" alt="Лого клиента"></div>
	<div class="clients__img-show clients__col clients__col--mobile clients__col_item--mobile"><img src="../assets/images/6.png" alt="Лого клиента"></div>
	<div class="clients__img-show clients__col clients__col--mobile clients__col_item--mobile"><img src="../assets/images/7.png" alt="Лого клиента"></div>
	<div class="clients__img-show clients__col clients__col--mobile clients__col_item--mobile"><img src="../assets/images/8.png" alt="Лого клиента"></div>
</div>
<!-- mobile row end -->
