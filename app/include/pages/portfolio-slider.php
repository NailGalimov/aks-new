<div class="portfolio__slider">
    <div class="portfolio__item">
        <div class="portfolio__item_title">
            2012
        </div>
        <div class="portfolio__item_text">
            <ul>
                <li> <span>Объект:</span><strong>ООО «Гринхауз»</strong></li>
                <li> <span>Geo:</span><strong>г. Старый Оскол</strong></li>
            </ul>
        </div>

        <div class="portfolio__item_results results">
            <div class="results__top">
                <p>Задача:</p>
                <p>поставка гофрированной трубы для строительства комбикормового завода</p>
            </div>
            <div class="results__bottom">
                <p>Результат:</p>
                <p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
            </div>
            <div class="results__price">
                25 0000 рублей
            </div>
        </div>
    </div>
    <div class="portfolio__item">
        <div class="portfolio__item_title">
            2013 - 2014
        </div>
        <div class="portfolio__item_text">
            <ul>
                <li> <span>Объект:</span><span><strong>ОАО «Мираторг»</strong></span></li>
                <li> <span>Geo:</span><span><strong>Пос. Кировский в Курской области</strong></span></li>
            </ul>
        </div>

        <div class="portfolio__item_results results">
            <div class="results__top">
                <p>Задача:</p>
                <p>поставка гофрированной трубы для строительства комбикормового завода</p>
            </div>
            <div class="results__bottom">
                <p>Результат:</p>
                <p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
            </div>
            <div class="results__price">
                25 0000 рублей
            </div>
        </div>
    </div>
    <div class="portfolio__item">
        <div class="portfolio__item_title">
            2018
        </div>
        <div class="portfolio__item_text">
            <ul>
                <li> <span>Объект:</span><span><strong>ГК «Агробелогорье»</strong></span></li>
                <li> <span>Geo:</span><span><strong>Борисовский район Белгородской области</strong></span></li>
            </ul>
        </div>

        <div class="portfolio__item_results results">
            <div class="results__top">
                <p>Задача:</p>
                <p>поставка гофрированной трубы для строительства комбикормового завода</p>
            </div>
            <div class="results__bottom">
                <p>Результат:</p>
                <p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
            </div>
            <div class="results__price">
                25 0000 рублей
            </div>
        </div>
    </div>
    <div class="portfolio__item">
        <div class="portfolio__item_title">
            2016
        </div>
        <div class="portfolio__item_text">
            <ul>
                <li> <span>Объект:</span><span><strong>ООО «Гринхауз»</strong></span></li>
                <li> <span>Geo:</span><span><strong>г. Старый Оскол</strong></span></li>
            </ul>
        </div>

        <div class="portfolio__item_results results">
            <div class="results__top">
                <p>Задача:</p>
                <p>поставка гофрированной трубы для строительства комбикормового завода</p>
            </div>
            <div class="results__bottom">
                <p>Результат:</p>
                <p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
            </div>
            <div class="results__price">
                25 0000 рублей
            </div>
        </div>
    </div>
    <div class="portfolio__item">
        <div class="portfolio__item_title">
            2020
        </div>
        <div class="portfolio__item_text">
            <ul>
                <li> <span>Объект:</span><span><strong>ООО «Гринхауз»</strong></span></li>
                <li> <span>Geo:</span><span><strong>г. Старый Оскол</strong></span></li>
            </ul>
        </div>

        <div class="portfolio__item_results results">
            <div class="results__top">
                <p>Задача:</p>
                <p>поставка гофрированной трубы для строительства комбикормового завода</p>
            </div>
            <div class="results__bottom">
                <p>Результат:</p>
                <p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
            </div>
            <div class="results__price">
                25 0000 рублей
            </div>
        </div>
    </div>
    <div class="portfolio__item">
        <div class="portfolio__item_title">
            2012
        </div>
        <div class="portfolio__item_text">
            <ul>
                <li> <span>Объект:</span><span><strong>ООО «Гринхауз»</strong></span></li>
                <li> <span>Geo:</span><span><strong>г. Старый Оскол</strong></span></li>
            </ul>
        </div>

        <div class="portfolio__item_results results">
            <div class="results__top">
                <p>Задача:</p>
                <p>поставка гофрированной трубы для строительства комбикормового завода</p>
            </div>
            <div class="results__bottom">
                <p>Результат:</p>
                <p>поставлена труба гофрированная ID 800 в объеме 4 800 погонных метров</p>
            </div>
            <div class="results__price">
                25 0000 рублей
            </div>
        </div>
    </div>
</div>
